export default class Material {

    constructor(shader, texture) {
        this.texture = texture;
        this.shader = shader;
        this.color = [1.0, 1.0, 1.0];
    }

    apply() {
        this.shader.enable();
        this.shader.uniforms.Albedo.set(this.texture);
        this.shader.uniforms.Blend.set(this.color);
    }
}