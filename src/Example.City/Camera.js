import { Matrix, Vector, Quaternion } from '../Engine/Math.js';

export default class Camera {

    constructor(transform) {
        this.transform = transform;
    }

    lookAt(eye, center, up) {
        let forward = center.subtract(eye).normalized();
        // let forward = eye.subtract(center).normalized();
        this.transform.rotation = Quaternion.lookRotation(forward, up);
        this.transform.position = eye.clone().negate();
        this.transform.update();
    }
}