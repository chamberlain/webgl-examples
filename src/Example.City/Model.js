import { EnsureArray } from "../Engine/Helpers.js";
import { Matrix } from "../Engine/Math.js";

export default class Model {

    constructor(meshes, material, transform) {
        this.meshes = EnsureArray(meshes);
        this.transform = transform;
        this.material = material;
    }

    get shader() {
        return this.material.shader;
    }

    draw(transform) {
        // 
        this.material.apply();

        // Assign model transform
        this.transform.update(); // Recompute transform
        this.shader.uniforms.Transform.set(this.transform.matrix);

        // Draw associated meshes
        for (let submesh of this.meshes)
            submesh.draw(this.shader);
    }
}