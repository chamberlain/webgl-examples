function Debounce(func, wait, immediate) {
    var timeout;
    return () => {
        var context = this, args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            timeout = null;
            if (!immediate) func.apply(context, args);
        }, wait);
        if (immediate && !timeout) func.apply(context, args);
    };
}

/**
 * Recursively convert array-like dicts to arrays.
 */
function Arrayify(node) {
    // If frozen, simply return the frozen object.
    if (Object.isFrozen(node)) return node;
    else {

        // Arrayify children
        for (let name in node)
            node[name] = Arrayify(node[name]);

        // Convert array-like objects with a length to actual array.
        if (typeof node === "object" && node.length)
            node = Array.from(node);

        return node;
    }
}

// 
let _randomSeed = new Date().getTime();
let _currentSeed = _randomSeed;

/**
 * Set a new random seed.
 * @param {Number} seed New random base seed
 */
function setRandomSeed(seed) {
    _currentSeed = _randomSeed = seed;
    
    _currentSeed = _currentSeed % 2147483647;
    if (_currentSeed <= 0)
        _currentSeed += 2147483646;
}

/**
 * Get the current random seed.
 */


/**
 * Get the next random number.
 */
function random() {
    _currentSeed = _currentSeed * 16807 % 2147483647;
    return _currentSeed / 2147483646;
}

/**
 * Ensures the given value is an array. 
 * - If already an array, it is simply returned. 
 * - If it is not an array, returns the value wrapped by an array.
 * @param {*} item Some item or array of items
 */
function EnsureArray(item) {
    if (Array.isArray(item)) return item;
    else return [item];
}

// Yup
const getTypeOf = o => {
    if (Array.isArray(o)) return "array";
    else return typeof o;
};

let ContextMap = {};

/**
 * Gets the most modern WebGL context available.
 * @param {HTMLCanvasElement} canvas Some canvas
 */
function getGraphicsContext(canvas) {
    // Try to get existing context
    if (canvas in ContextMap) return ContextMap[canvas];
    // Unable to find existing context, get it from the canvas.
    else {
        // Try to get a WebGL 2 Context
        // let context = canvas.getContext("webgl2");
        // if (context) {
        //     ContextMap[canvas] = context;
        //     return context;
        // }

        // Try to get a WebGL 1 Context
        let context = canvas.getContext("webgl");
        if (context) {
            ContextMap[canvas] = context;
            return context;
        }

        // No WebGL, Damn!
        throw new Error("Unable to acquire WebGL 2 context.");
    }
}

function getCanvasElement(selector) {
    if (selector instanceof HTMLCanvasElement) return selector;
    else return document.querySelector(selector);
}

//
let GL = undefined;
let Canvas = undefined;

let Context = {
    /** 
     * The current WebGL context 
     */
    get GL() {
        return GL;
    },
    // Get the canvas
    get Canvas() {
        return Canvas;
    },
    //
    SetCanvas(selector, autoResize = true) {
        // Check if the context was already initialized
        if (GL) throw new Error(`GL already initialized on ${Canvas}.`);

        // Get canvas element
        Canvas = getCanvasElement(selector);

        // Get context
        GL = getGraphicsContext(Canvas);

        // Backface Culling
        // GL.enable(GL.CULL_FACE);

        // Z-Buffer ( Depth Testing )
        GL.enable(GL.DEPTH_TEST);
        GL.depthFunc(GL.LEQUAL);
    },

    SetFullscreen(selector) {
        // Find canvas element
        let canvas = getCanvasElement(selector);

        window.addEventListener("resize", Debounce(() => {
            canvas.width = window.innerWidth / (1*window.devicePixelRatio || 1);
            canvas.height = window.innerHeight / (1*window.devicePixelRatio || 1);
        }, 50));

        // Trigger resize for initial adjustment
        window.dispatchEvent(new Event("resize"));
    },

    SetRenderCallback(renderCallback) {
        window.requestAnimationFrame(function renderLoop() {
            // Request to render again next frame
            window.requestAnimationFrame(renderLoop);
            renderCallback();
        });
    }
};

const isPowerOf2 = (value) => {
    return (value & (value - 1)) == 0;
};

/**
 * A 2D Texture
 */
class Texture {

    /**
     * Creates a new 2D Texture.
     * @param {Image} image Some HTML image
     */
    constructor(image) {
        this.texture = GL.createTexture();
        if (image) this.update(image);
    }

    /**
     * Binds the current texture the given unit.
     * @param {Number} [unit=0] Texture Unit
     */
    use(unit = 0) {
        GL.activeTexture(GL.TEXTURE0 + unit);
        GL.bindTexture(GL.TEXTURE_2D, this.texture);
    }

    /**
     * Updates the texture with new data.
     */
    update(image) {

        GL.bindTexture(GL.TEXTURE_2D, this.texture);
        GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, GL.RGBA, GL.UNSIGNED_BYTE, image);

        // Create mimaps
        if (isPowerOf2(image.width) && isPowerOf2(image.height))
            GL.generateMipmap(GL.TEXTURE_2D);

        GL.bindTexture(GL.TEXTURE_2D, null);
    }
}

// Modified ES6 Port of https://github.com/evanw/lightgl.js/blob/master/src/vector.js
class Vector {
    /**
     * Creates a new 3d vector.
     * @param {Number} [x] X Coordinate
     * @param {Number} [y] Y Coordinate
     * @param {Number} [z] Z Coordinate
     */
    constructor(x, y, z) {
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
    }

    static get UnitX() { return UNIT_X; }

    static get UnitY() { return UNIT_Y; }

    static get UnitZ() { return UNIT_Z; }

    static get One() { return ONE; }

    static get Zero() { return ZERO; }

    set(x, y, z) {
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
    }

    add(v) {
        if (v instanceof Vector) return new Vector(this.x + v.x, this.y + v.y, this.z + v.z);
        else return new Vector(this.x + v, this.y + v, this.z + v);
    }

    subtract(v) {
        if (v instanceof Vector) return new Vector(this.x - v.x, this.y - v.y, this.z - v.z);
        else return new Vector(this.x - v, this.y - v, this.z - v);
    }

    multiply(v) {
        if (v instanceof Vector) return new Vector(this.x * v.x, this.y * v.y, this.z * v.z);
        else return new Vector(this.x * v, this.y * v, this.z * v);
    }

    divide(v) {
        if (v instanceof Vector) return new Vector(this.x / v.x, this.y / v.y, this.z / v.z);
        else return new Vector(this.x / v, this.y / v, this.z / v);
    }

    equals(v) {
        return this.x == v.x && this.y == v.y && this.z == v.z;
    }

    dot(v) {
        return this.x * v.x + this.y * v.y + this.z * v.z;
    }

    cross(v) {
        return new Vector(this.y * v.z - this.z * v.y, this.z * v.x - this.x * v.z, this.x * v.y - this.y * v.x);
    }

    negate() {
        return new Vector(-this.x, -this.y, -this.z);
    }

    get length() {
        return Math.sqrt(this.dot(this));
    }

    normalized(result = undefined) {
        result = result || new Vector();
        return Vector.normalize(this, result);
    }

    min() {
        return Math.min(Math.min(this.x, this.y), this.z);
    }

    max() {
        return Math.max(Math.max(this.x, this.y), this.z);
    }

    toAngles() {
        return {
            theta: Math.atan2(this.z, this.x),
            phi: Math.asin(this.y / this.length())
        };
    }

    angleTo(a) {
        return Math.acos(this.dot(a) / (this.length() * a.length()));
    }

    toArray(n) {
        return [this.x, this.y, this.z].slice(0, n || 3);
    }

    clone() {
        return new Vector(this.x, this.y, this.z);
    }

    set(x, y, z) {
        this.x = x;
        this.y = y;
        this.z = z;
        return this;
    }

    static fromAngles(theta, phi) {
        return new Vector(Math.cos(theta) * Math.cos(phi), Math.sin(phi), Math.sin(theta) * Math.cos(phi));
    }

    static fromArray(a) {
        return new Vector(a[0], a[1], a[2]);
    }

    static orthoNormalize(f, u) {
        f = f.normalized();
        let v = Vector.normalize(f.cross(u));
        return v.cross(f);
    }

    static randomDirection() {
        return Vector.fromAngles(Math.random() * Math.PI * 2, Math.asin(Math.random() * 2 - 1));
    }

    static min(a, b) {
        return new Vector(Math.min(a.x, b.x), Math.min(a.y, b.y), Math.min(a.z, b.z));
    }

    static max(a, b) {
        return new Vector(Math.max(a.x, b.x), Math.max(a.y, b.y), Math.max(a.z, b.z));
    }

    static normalize(v, result = undefined) {
        result = result || new Vector();

        const m = 1.0 / v.length;

        // Store normalized vector
        result.x = v.x * m;
        result.y = v.y * m;
        result.z = v.z * m;

        return result;
    }
}

const ZERO = Object.freeze(new Vector(0, 0, 0));
const UNIT_X = Object.freeze(new Vector(1, 0, 0));
const UNIT_Y = Object.freeze(new Vector(0, 1, 0));
const UNIT_Z = Object.freeze(new Vector(0, 0, 1));
const ONE = Object.freeze(new Vector(1, 1, 1));

// Modified ES6 Port of https://github.com/evanw/lightgl.js/blob/master/src/vector.js
class Quaternion {
    /**
     * Creates a new quaternion.
     * @param {Number} [x] X Component
     * @param {Number} [y] Y Component
     * @param {Number} [z] Z Component
     * @param {Number} [w] W Component
     */
    constructor(x, y, z, w) {
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
        this.w = w || 0;
    }

    multiply(v) {
        return Quaternion.multiply(this, v);
    }

    equals(v) {
        return this.x == v.x
            && this.y == v.y
            && this.z == v.z
            && this.w == v.w;
    }

    normalized() {
        return Quaternion.normalize(this);
    }

    get length() {
        let sqrLength = (this.x * this.x) + (this.y * this.y) + (this.z * this.z) + (this.w * this.w);
        return Math.sqrt(sqrLength);
    }

    clone() {
        return new Quaternion(this.x, this.y, this.z, this.w);
    }

    static identity(result) {
        result = result || new Quaternion();
        result.x = result.y = result.z = 0;
        result.w = 1;
        return result;
    }

    static multiply(a, b, result) {
        result = result || new Quaternion();

        let ax = a.x, ay = a.y, az = a.z, aw = a.w;
        let bx = b.x, by = b.y, bz = b.z, bw = b.w;

        result.x = ax * bw + aw * bx + ay * bz - az * by;
        result.y = ay * bw + aw * by + az * bx - ax * bz;
        result.z = az * bw + aw * bz + ax * by - ay * bx;
        result.w = aw * bw - ax * bx - ay * by - az * bz;

        return result;
    }

    static toAxisAngle(q, result) {
        result = result || new Vector();

        let rad = Math.acos(q.w) * 2.0;
        let s = Math.sin(rad / 2.0);

        if (s != 0.0) {
            result.x = q.x / s;
            result.y = q.y / s;
            result.z = q.z / s;
        } else {
            // If s is zero, return any axis (no rotation - axis does not matter)
            result.x = 1;
            result.y = 0;
            result.z = 0;
        }

        return { axis: result, angle: rad };
    }

    static fromAxisAngle(axis, angle, result) {
        result = result || new Quaternion();

        angle = angle * 0.5;

        let s = Math.sin(angle);
        result.x = s * axis.x;
        result.y = s * axis.y;
        result.z = s * axis.z;
        result.w = Math.cos(angle);

        return result;
    }

    static rotateTo(from, dest) {
        let m = Math.sqrt(2.0 + 2.0 * from.dot(dest));
        let w = from.cross(dest).multiply(1.0 / m);
        return new Quaternion(w.x, w.y, w.z, 0.5 * m);
    }

    static lookRotation(f, u, result) {
        result = result || new Quaternion();
        u = u || Vector.UnitZ;

        // 
        u = Vector.orthoNormalize(f, u);
        let r = u.cross(f);

        result.w = Math.sqrt(1.0 + r.x + u.y + f.z) * 0.5;

        let w4_recip = 1.0 / (4.0 * result.w);
        result.x = (u.z - f.y) * w4_recip;
        result.y = (f.x - r.z) * w4_recip;
        result.z = (r.y - u.x) * w4_recip;

        return result;
    }


    static normalize(q) {

        let m = 1.0 / q.length;

        let x = q.x * m;
        let y = q.y * m;
        let z = q.z * m;
        let w = q.w * m;

        return new Quaternion(x, y, z, w);
    }

    static slerp(a, b, t, result) {
        result = result || new Quaternion();

        let ax = a.x, ay = a.y, az = a.z, aw = a.w;
        let bx = b.x, by = b.y, bz = b.z, bw = b.w;

        let omega, cosom, sinom, scale0, scale1;

        // calc cosine
        cosom = ax * bx + ay * by + az * bz + aw * bw;

        // adjust signs (if necessary)
        if (cosom < 0.0) {
            cosom = -cosom;
            bx = - bx;
            by = - by;
            bz = - bz;
            bw = - bw;
        }

        // calculate coefficients
        if ((1.0 - cosom) > 0.000001) {
            // standard case (slerp)
            omega = Math.acos(cosom);
            sinom = Math.sin(omega);
            scale0 = Math.sin((1.0 - t) * omega) / sinom;
            scale1 = Math.sin(t * omega) / sinom;
        } else {
            // "from" and "to" quaternions are very close
            //  ... so we can do a linear interpolation
            scale0 = 1.0 - t;
            scale1 = t;
        }
        // calculate final values
        result.x = scale0 * ax + scale1 * bx;
        result.y = scale0 * ay + scale1 * by;
        result.z = scale0 * az + scale1 * bz;
        result.w = scale0 * aw + scale1 * bw;

        return result;
    }

    static invert(a, result) {
        result = result || new Quaternion();

        let a0 = a.x, a1 = a.y, a2 = a.z, a3 = a.w;
        let dot = a0 * a0 + a1 * a1 + a2 * a2 + a3 * a3;

        if (dot == 0.0) {
            result.x = 0;
            result.y = 0;
            result.z = 0;
            result.w = 0;
        } else {
            let invDot = dot ? 1.0 / dot : 0;

            result.x = -a0 * invDot;
            result.x = -a1 * invDot;
            result.z = -a2 * invDot;
            result.w = a3 * invDot;
        }

        return result;
    }

    static conjugate(a, result) {
        result = result || new Quaternion();

        result.x = -a.x;
        result.y = -a.y;
        result.z = -a.z;
        result.w = a.w;

        return result;
    }
}

const IDENTITY = Object.freeze(new Quaternion(0, 0, 0, 1));

// ES6 Port of https://github.com/evanw/lightgl.js/blob/master/src/matrix.js

var hasFloat32Array = typeof Float32Array != "undefined";

class Matrix {

    /**
     * Creates a new 4x4 matrix.
     */
    constructor() {
        var m = Array.prototype.concat.apply([], arguments);
        if (!m.length) {
            m = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1];
        }

        this.m = hasFloat32Array ? new Float32Array(m) : m;
    }

    // Returns the matrix that when multiplied with this matrix results in the
    // identity matrix.
    inverse() {
        return Matrix.inverse(this, new Matrix());
    }

    // Returns this matrix, exchanging columns for rows.
    transpose() {
        return Matrix.transpose(this, new Matrix());
    }

    // Returns the concatenation of the transforms for this matrix and `matrix`.
    // This emulates the OpenGL function `glMultMatrix()`.
    multiply(matrix) {
        return Matrix.multiply(this, matrix, new Matrix());
    }

    // Transforms the vector as a point with a w coordinate of 1. This
    // means translations will have an effect, for example.
    transformPoint(v) {
        var m = this.m;
        return new Vector(
            m[0] * v.x + m[1] * v.y + m[2] * v.z + m[3],
            m[4] * v.x + m[5] * v.y + m[6] * v.z + m[7],
            m[8] * v.x + m[9] * v.y + m[10] * v.z + m[11]
        ).divide(m[12] * v.x + m[13] * v.y + m[14] * v.z + m[15]);
    }

    // Transforms the vector as a vector with a w coordinate of 0. This
    // means translations will have no effect, for example.
    transformVector(v) {
        var m = this.m;
        return new Vector(
            m[0] * v.x + m[1] * v.y + m[2] * v.z,
            m[4] * v.x + m[5] * v.y + m[6] * v.z,
            m[8] * v.x + m[9] * v.y + m[10] * v.z
        );
    }

    // Returns the matrix that when multiplied with `matrix` results in the
    // identity matrix. You can optionally pass an existing matrix in `result`
    // to avoid allocating a new matrix. This implementation is from the Mesa
    // OpenGL function `__gluInvertMatrixd()` found in `project.c`.
    static inverse(matrix, result) {
        result = result || new Matrix();

        // let a = matrix.m;

        // let a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3];
        // let a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7];
        // let a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11];
        // let a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15];

        // let b00 = a00 * a11 - a01 * a10;
        // let b01 = a00 * a12 - a02 * a10;
        // let b02 = a00 * a13 - a03 * a10;
        // let b03 = a01 * a12 - a02 * a11;
        // let b04 = a01 * a13 - a03 * a11;
        // let b05 = a02 * a13 - a03 * a12;
        // let b06 = a20 * a31 - a21 * a30;
        // let b07 = a20 * a32 - a22 * a30;
        // let b08 = a20 * a33 - a23 * a30;
        // let b09 = a21 * a32 - a22 * a31;
        // let b10 = a21 * a33 - a23 * a31;
        // let b11 = a22 * a33 - a23 * a32;

        // // Calculate the determinant
        // let det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

        // if (!det) {
        //     return null;
        // }
        // det = 1.0 / det;

        // let out = result.m;
        // out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
        // out[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
        // out[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
        // out[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
        // out[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
        // out[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
        // out[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
        // out[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
        // out[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
        // out[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
        // out[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
        // out[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
        // out[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
        // out[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
        // out[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
        // out[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;

        // return result;

        var m = matrix.m, r = result.m;

        r[0] = m[5] * m[10] * m[15] - m[5] * m[14] * m[11] - m[6] * m[9] * m[15] + m[6] * m[13] * m[11] + m[7] * m[9] * m[14] - m[7] * m[13] * m[10];
        r[1] = -m[1] * m[10] * m[15] + m[1] * m[14] * m[11] + m[2] * m[9] * m[15] - m[2] * m[13] * m[11] - m[3] * m[9] * m[14] + m[3] * m[13] * m[10];
        r[2] = m[1] * m[6] * m[15] - m[1] * m[14] * m[7] - m[2] * m[5] * m[15] + m[2] * m[13] * m[7] + m[3] * m[5] * m[14] - m[3] * m[13] * m[6];
        r[3] = -m[1] * m[6] * m[11] + m[1] * m[10] * m[7] + m[2] * m[5] * m[11] - m[2] * m[9] * m[7] - m[3] * m[5] * m[10] + m[3] * m[9] * m[6];

        r[4] = -m[4] * m[10] * m[15] + m[4] * m[14] * m[11] + m[6] * m[8] * m[15] - m[6] * m[12] * m[11] - m[7] * m[8] * m[14] + m[7] * m[12] * m[10];
        r[5] = m[0] * m[10] * m[15] - m[0] * m[14] * m[11] - m[2] * m[8] * m[15] + m[2] * m[12] * m[11] + m[3] * m[8] * m[14] - m[3] * m[12] * m[10];
        r[6] = -m[0] * m[6] * m[15] + m[0] * m[14] * m[7] + m[2] * m[4] * m[15] - m[2] * m[12] * m[7] - m[3] * m[4] * m[14] + m[3] * m[12] * m[6];
        r[7] = m[0] * m[6] * m[11] - m[0] * m[10] * m[7] - m[2] * m[4] * m[11] + m[2] * m[8] * m[7] + m[3] * m[4] * m[10] - m[3] * m[8] * m[6];

        r[8] = m[4] * m[9] * m[15] - m[4] * m[13] * m[11] - m[5] * m[8] * m[15] + m[5] * m[12] * m[11] + m[7] * m[8] * m[13] - m[7] * m[12] * m[9];
        r[9] = -m[0] * m[9] * m[15] + m[0] * m[13] * m[11] + m[1] * m[8] * m[15] - m[1] * m[12] * m[11] - m[3] * m[8] * m[13] + m[3] * m[12] * m[9];
        r[10] = m[0] * m[5] * m[15] - m[0] * m[13] * m[7] - m[1] * m[4] * m[15] + m[1] * m[12] * m[7] + m[3] * m[4] * m[13] - m[3] * m[12] * m[5];
        r[11] = -m[0] * m[5] * m[11] + m[0] * m[9] * m[7] + m[1] * m[4] * m[11] - m[1] * m[8] * m[7] - m[3] * m[4] * m[9] + m[3] * m[8] * m[5];

        r[12] = -m[4] * m[9] * m[14] + m[4] * m[13] * m[10] + m[5] * m[8] * m[14] - m[5] * m[12] * m[10] - m[6] * m[8] * m[13] + m[6] * m[12] * m[9];
        r[13] = m[0] * m[9] * m[14] - m[0] * m[13] * m[10] - m[1] * m[8] * m[14] + m[1] * m[12] * m[10] + m[2] * m[8] * m[13] - m[2] * m[12] * m[9];
        r[14] = -m[0] * m[5] * m[14] + m[0] * m[13] * m[6] + m[1] * m[4] * m[14] - m[1] * m[12] * m[6] - m[2] * m[4] * m[13] + m[2] * m[12] * m[5];
        r[15] = m[0] * m[5] * m[10] - m[0] * m[9] * m[6] - m[1] * m[4] * m[10] + m[1] * m[8] * m[6] + m[2] * m[4] * m[9] - m[2] * m[8] * m[5];

        var det = m[0] * r[0] + m[1] * r[4] + m[2] * r[8] + m[3] * r[12];
        for (var i = 0; i < 16; i++) r[i] /= det;
        return result;
    }

    // Returns `matrix`, exchanging columns for rows. You can optionally pass an
    // existing matrix in `result` to avoid allocating a new matrix.
    static transpose(matrix, result) {
        result = result || new Matrix();
        var m = matrix.m, r = result.m;
        r[0] = m[0]; r[1] = m[4]; r[2] = m[8]; r[3] = m[12];
        r[4] = m[1]; r[5] = m[5]; r[6] = m[9]; r[7] = m[13];
        r[8] = m[2]; r[9] = m[6]; r[10] = m[10]; r[11] = m[14];
        r[12] = m[3]; r[13] = m[7]; r[14] = m[11]; r[15] = m[15];
        return result;
    }

    // Returns the concatenation of the transforms for `left` and `right`. You can
    // optionally pass an existing matrix in `result` to avoid allocating a new
    // matrix.
    static multiply(left, right, result) {
        result = result || new Matrix();

        var a = left.m, b = right.m, r = result.m;

        r[0] = a[0] * b[0] + a[1] * b[4] + a[2] * b[8] + a[3] * b[12];
        r[1] = a[0] * b[1] + a[1] * b[5] + a[2] * b[9] + a[3] * b[13];
        r[2] = a[0] * b[2] + a[1] * b[6] + a[2] * b[10] + a[3] * b[14];
        r[3] = a[0] * b[3] + a[1] * b[7] + a[2] * b[11] + a[3] * b[15];

        r[4] = a[4] * b[0] + a[5] * b[4] + a[6] * b[8] + a[7] * b[12];
        r[5] = a[4] * b[1] + a[5] * b[5] + a[6] * b[9] + a[7] * b[13];
        r[6] = a[4] * b[2] + a[5] * b[6] + a[6] * b[10] + a[7] * b[14];
        r[7] = a[4] * b[3] + a[5] * b[7] + a[6] * b[11] + a[7] * b[15];

        r[8] = a[8] * b[0] + a[9] * b[4] + a[10] * b[8] + a[11] * b[12];
        r[9] = a[8] * b[1] + a[9] * b[5] + a[10] * b[9] + a[11] * b[13];
        r[10] = a[8] * b[2] + a[9] * b[6] + a[10] * b[10] + a[11] * b[14];
        r[11] = a[8] * b[3] + a[9] * b[7] + a[10] * b[11] + a[11] * b[15];

        r[12] = a[12] * b[0] + a[13] * b[4] + a[14] * b[8] + a[15] * b[12];
        r[13] = a[12] * b[1] + a[13] * b[5] + a[14] * b[9] + a[15] * b[13];
        r[14] = a[12] * b[2] + a[13] * b[6] + a[14] * b[10] + a[15] * b[14];
        r[15] = a[12] * b[3] + a[13] * b[7] + a[14] * b[11] + a[15] * b[15];

        return result;
    }

    // Returns an identity matrix. You can optionally pass an existing matrix in
    // `result` to avoid allocating a new matrix.
    static identity(result) {
        result = result || new Matrix();
        var m = result.m;
        m[0] = m[5] = m[10] = m[15] = 1;
        m[1] = m[2] = m[3] = m[4] = m[6] = m[7] = m[8] = m[9] = m[11] = m[12] = m[13] = m[14] = 0;
        return result;
    }

    // Returns a perspective transform matrix, which makes far away objects appear
    // smaller than nearby objects. The `aspect` argument should be the width
    // divided by the height of your viewport and `fov` is the top-to-bottom angle
    // of the field of view in radians. You can optionally pass an existing matrix
    // in `result` to avoid allocating a new matrix.
    static perspective(fovy, aspect, near, far, result) {

        result = result || new Matrix();
        var out = result.m;

        let f = 1.0 / Math.tan(fovy / 2);
        let nf = 1 / (near - far);
        out[0] = f / aspect;
        // out[1] = 0;
        // out[2] = 0;
        // out[3] = 0;
        // out[4] = 0;
        out[5] = f;
        // out[6] = 0;
        // out[7] = 0;
        // out[8] = 0;
        // out[9] = 0;
        out[10] = (far + near) * nf;
        out[11] = -1;
        // out[12] = 0;
        // out[13] = 0;
        out[14] = (2 * far * near) * nf;
        // out[15] = 0;

        return result;
    }

    // Returns an orthographic projection, in which objects are the same size no
    // matter how far away or nearby they are. You can optionally pass an existing
    // matrix in `result` to avoid allocating a new matrix. This emulates the OpenGL
    // function `glOrtho()`.
    static ortho(l, r, b, t, n, f, result) {
        result = result || new Matrix();
        var m = result.m;

        m[0] = 2 / (r - l);
        m[1] = 0;
        m[2] = 0;
        m[3] = -(r + l) / (r - l);

        m[4] = 0;
        m[5] = 2 / (t - b);
        m[6] = 0;
        m[7] = -(t + b) / (t - b);

        m[8] = 0;
        m[9] = 0;
        m[10] = -2 / (f - n);
        m[11] = -(f + n) / (f - n);

        m[12] = 0;
        m[13] = 0;
        m[14] = 0;
        m[15] = 1;

        return result;
    }

    // Returns a matrix that puts the camera at the eye point looking
    // toward the center point.
    // You can optionally pass an existing matrix in `result` to avoid allocating
    // a new matrix.
    static lookAt(eye, center, up, result) {
        result = result || new Matrix();
        up = up || Vector.UnitZ;

        let z = eye.subtract(center).normalized();
        let x = up.cross(z).normalized();
        let y = z.cross(x).normalized();

        let m = result.m;

        m[0] = x.x;
        m[1] = y.x;
        m[2] = z.x;
        m[3] = 0;

        m[4] = x.y;
        m[5] = y.y;
        m[6] = z.y;
        m[7] = 0;

        m[8] = x.z;
        m[9] = y.z;
        m[10] = z.z;
        m[11] = 0;

        m[12] = -x.dot(eye);
        m[13] = -y.dot(eye);
        m[14] = -z.dot(eye);
        m[15] = 1;

        return result;
    }

    // This emulates the OpenGL function `glScale()`. You can optionally pass an
    // existing matrix in `result` to avoid allocating a new matrix.
    static scale(x, y, z, result) {
        result = result || new Matrix();

        var m = result.m;

        m[0] = x;
        m[1] = 0;
        m[2] = 0;
        m[3] = 0;

        m[4] = 0;
        m[5] = y;
        m[6] = 0;
        m[7] = 0;

        m[8] = 0;
        m[9] = 0;
        m[10] = z;
        m[11] = 0;

        m[12] = 0;
        m[13] = 0;
        m[14] = 0;
        m[15] = 1;

        return result;
    }

    // This emulates the OpenGL function `glTranslate()`. You can optionally pass
    // an existing matrix in `result` to avoid allocating a new matrix.
    static translate(x, y, z, result) {
        result = result || new Matrix();
        var m = result.m;

        m[0] = 1;
        m[1] = 0;
        m[2] = 0;
        m[3] = 0;

        m[4] = 0;
        m[5] = 1;
        m[6] = 0;
        m[7] = 0;

        m[8] = 0;
        m[9] = 0;
        m[10] = 1;
        m[11] = 0;

        m[12] = x;
        m[13] = y;
        m[14] = z;
        m[15] = 1;

        return result;
    }

    // Returns a matrix that rotates by `angle` radians around the vector `x, y, z`.
    // You can optionally pass an existing matrix in `result` to avoid allocating
    // a new matrix. This emulates the OpenGL function `glRotate()`.
    static rotate(angle, x, y, z, result) {
        if (!angle || (!x && !y && !z)) {
            return Matrix.identity(result);
        }

        result = result || new Matrix();
        var m = result.m;

        var d = Math.sqrt(x * x + y * y + z * z);
        x /= d; y /= d; z /= d;
        var c = Math.cos(angle), s = Math.sin(angle), t = 1 - c;

        m[0] = x * x * t + c;
        m[1] = x * y * t - z * s;
        m[2] = x * z * t + y * s;
        m[3] = 0;

        m[4] = y * x * t + z * s;
        m[5] = y * y * t + c;
        m[6] = y * z * t - x * s;
        m[7] = 0;

        m[8] = z * x * t - y * s;
        m[9] = z * y * t + x * s;
        m[10] = z * z * t + c;
        m[11] = 0;

        m[12] = 0;
        m[13] = 0;
        m[14] = 0;
        m[15] = 1;

        return result;
    }

    static fromQuaternion(q, result) {
        result = result || Matrix.identity();

        let r = Quaternion.toAxisAngle(q);
        return Matrix.rotate(r.angle, r.axis.x, r.axis.y, r.axis.z);
    }
}

class Uniform {
    
        constructor(info) {
            this.info = info;
            this.value = undefined;
            this.changed = false;
        }
    
        get() {
            return this.value;
        }
    
        set(val) {
            this.changed = true;
            this.value = val;
        }
    
        update() {
            switch (this.info.type) {
    
                // Float
                case "float":
                    if (this.info.size > 1) GL.uniform1fv(this.info.location, this.value);
                    else GL.uniform1f(this.info.location, this.value);
                    break;
    
                case "vec2":
                    if ('x' in this.value) GL.uniform2f(this.info.location, this.value.x, this.value.y);
                    else GL.uniform2fv(this.info.location, this.value);
                    break;
    
                case "vec3":
                    if ('x' in this.value) GL.uniform3f(this.info.location, this.value.x, this.value.y, this.value.z);
                    else GL.uniform3fv(this.info.location, this.value);
                    break;
    
                case "vec4":
                    if ('x' in this.value) GL.uniform4f(this.info.location, this.value.x, this.value.y, this.value.z, this.value.w);
                    else GL.uniform4fv(this.info.location, this.value);
                    break;
    
                // Integer 
                case "int":
                    if (this.info.size > 1) GL.uniform1iv(this.info.location, this.value);
                    else GL.uniform1i(this.info.location, this.value);
                    break;
    
                case "ivec2":
                    if ('x' in this.value) GL.uniform2i(this.info.location, this.value.x, this.value.y);
                    else GL.uniform2iv(this.info.location, this.value);
                    break;
    
                case "ivec3":
                    if ('x' in this.value) GL.uniform3i(this.info.location, this.value.x, this.value.y, this.value.z);
                    else GL.uniform3iv(this.info.location, this.value);
                    break;
    
                case "ivec4":
                    if ('x' in this.value) GL.uniform4i(this.info.location, this.value.x, this.value.y, this.value.z, this.value.w);
                    else GL.uniform4iv(this.info.location, this.value);
                    break;
    
                // Boolean
                case "bool":
                    if (this.info.size > 1) GL.uniform1iv(this.info.location, this.value);
                    else GL.uniform1i(this.info.location, this.value);
                    break;
    
                case "bvec2":
                    if ('x' in this.value) GL.uniform2i(this.info.location, this.value.x, this.value.y);
                    else GL.uniform2iv(this.info.location, this.value);
                    break;
    
                case "bvec3":
                    if ('x' in this.value) GL.uniform3i(this.info.location, this.value.x, this.value.y, this.value.z);
                    else GL.uniform3iv(this.info.location, this.value);
                    break;
    
                case "bvec4":
                    if ('x' in this.value) GL.uniform4i(this.info.location, this.value.x, this.value.y, this.value.z, this.value.w);
                    else GL.uniform4iv(this.info.location, this.value);
                    break;
    
                // Matrix
                case "mat2":
                    if ('m' in this.value) GL.uniformMatrix2fv(this.info.location, false, this.value.m);
                    else GL.uniformMatrix2fv(this.info.location, false, this.value);
                    break;
    
                case "mat3":
                    if ('m' in this.value) GL.uniformMatrix3fv(this.info.location, false, this.value.m);
                    else GL.uniformMatrix3fv(this.info.location, false, this.value);
                    break;
    
                case "mat4":
                    if ('m' in this.value) GL.uniformMatrix4fv(this.info.location, false, this.value.m);
                    else GL.uniformMatrix4fv(this.info.location, false, this.value);
                    break;
    
                // Texture
                case "sampler2D":
                    // May not be the best way, but DOES work everytime.
                    // Change to active texture
                    GL.uniform1i(this.info.location, this.info.textureUnit);
                    // Bind texture in correct unit
                    GL.activeTexture(GL.TEXTURE0 + this.info.textureUnit);
                    GL.bindTexture(GL.TEXTURE_2D, this.value.texture);
                    break;
    
                case "samplerCube":
                    // May not be the best way, but DOES work everytime.
                    // Change to active texture
                    GL.uniform1i(this.info.location, this.info.textureUnit);
                    // Bind texture in correct unit
                    GL.activeTexture(GL.TEXTURE0 + this.info.textureUnit);
                    GL.bindTexture(GL.TEXTURE_CUBE_MAP, this.value.texture);
                    break;
            }
    
            // Mark as updated
            this.changed = false;
        }
    }

/** Maps GL constants with friendly GLSL types. */
const GLSLTypeMap = {
    5124: "int", 5126: "float", 35670: "bool",
    35664: "vec2", 35665: "vec3", 35666: "vec4",
    35667: "ivec2", 35668: "ivec3", 35669: "ivec4",
    35671: "bvec2", 35672: "bvec3", 35673: "bvec4",
    35674: "mat2", 35675: "mat3", 35676: "mat4",
    35678: "sampler2D", 35680: "samplerCube"
};

// Make bi-directional mapping of the types
for (let k in GLSLTypeMap)
    GLSLTypeMap[GLSLTypeMap[k]] = +k;

/** Maps friendly GLSL types to their respective component count. */
const GLSLTypeSize = {
    "int": 1, "float": 1, "bool": 1,
    "vec2": 2, "vec3": 3, "vec4": 4,
    "ivec2": 2, "ivec3": 3, "ivec4": 4,
    "bvec2": 2, "bvec3": 3, "bvec4": 4,
    "sampler2D": 1, "samplerCube": 1
};

/** Maps friendly GLSL types to their primitive base type. */


/**
 * Compiles a GLSL Shader.
 * @param {String} sourceCode GLSL ES Source Code
 * @param {Number} type GL Constant ( Either FRAGMENT_SHADER or VERTEX_SHADER )
 */
function compileShader(sourceCode, type) {

    // Create shader
    var shader = GL.createShader(type);

    // Set and compile shader source
    GL.shaderSource(shader, sourceCode);
    GL.compileShader(shader);

    // Check compile status
    if (!GL.getShaderParameter(shader, GL.COMPILE_STATUS)) {
        var info = GL.getShaderInfoLog(shader);
        GL.deleteShader(shader); // Remove shader, didn't compile.
        throw "Could not compile GLSL Shader. \n\n" + info;
    }

    // 
    return shader;
}

/**
 * Links the given shaders together into a single program.
 * @param {Number} vertexShader Handle to GLSL Vertex Shader
 * @param {Number} fragmentShader Handle to GLSL Fragment Shader
 */
function compileProgram(vertexShader, fragmentShader) {

    // Link Programs
    let program = GL.createProgram();

    // Attach pre-existing shaders
    GL.attachShader(program, vertexShader);
    GL.attachShader(program, fragmentShader);
    GL.linkProgram(program);

    // Check compile status
    if (!GL.getProgramParameter(program, GL.LINK_STATUS)) {
        var info = GL.getProgramInfoLog(program);
        GL.deleteProgram(program); // Remove program, didn't link.
        throw "Could not compile GLSL Program. \n\n" + info;
    }

    return program;
}

/**
 * Extracts the segments of the uniform path.
 * ie. Splits "A.B[2].C" to [{A}, {B,2}, {C}]
 */
function extractSegments(info) {

    let path = info.name;

    let segments = [];
    while (path.length > 0) {

        // 
        let structPos = path.indexOf(".");

        // 
        let identifier = structPos >= 0 ? path.substr(0, structPos) : path;
        let access = undefined;

        // BUG: 'A.B[0].C' doesn't split properly.

        // 
        let arrayPos = path.indexOf("[");
        if (arrayPos >= 0) {
            // 
            let endArrayPos = identifier.indexOf("]");
            access = +identifier.substr(arrayPos + 1, endArrayPos - arrayPos - 1);

            // 
            identifier = identifier.substr(0, arrayPos);

            // Chop off end of path
            if (structPos >= 0) path = path.substr(structPos + 1);
            else path = path.substr(endArrayPos + 1);
        } else {
            // 
            name = path;
            // 
            if (structPos >= 0) path = path.substr(structPos + 1);
            else path = "";
        }

        // 
        let segment = { identifier };
        if (access !== undefined)
            segment.access = access;

        // 
        segments.push(segment);
    }

    return segments;
}

function extractFromProgram(program, count, getInfo, getLocation) {

    let attributes = [];

    // Enumerate uniforms and store information about each
    for (var index = 0; index < count; index++) {

        let info = getInfo(program, index);

        // Get information for the i-th uniform
        attributes.push({
            location: getLocation(program, info.name),
            info
        });
    }

    return attributes;
}

function extractAttributes(program) {

    // Enumerate attributes and store information about each 
    let attributes = extractFromProgram(program,
        GL.getProgramParameter(program, GL.ACTIVE_ATTRIBUTES),
        GL.getActiveAttrib.bind(GL),
        GL.getAttribLocation.bind(GL)
    );

    let attribute_data = {};

    // 
    attributes.forEach((attribute, index) => {
        // Store Attribute Data
        attribute_data[attribute.info.name] = {
            location: attribute.location,
            info: Object.freeze({
                size: attribute.info.size,
                type: GLSLTypeMap[attribute.info.type],
                name: attribute.info.name
            })
        };
    });

    // Prevent modifications to the attribute information
    return Object.freeze(attribute_data);
}

function extractUniforms(program) {

    // Reads uniforms from GLSL
    let uniforms = extractFromProgram(program,
        GL.getProgramParameter(program, GL.ACTIVE_UNIFORMS),
        GL.getActiveUniform.bind(GL),
        GL.getUniformLocation.bind(GL)
    );

    let uniform_data = {};

    let unit = 0;
    for (let uniform of uniforms) {

        let info = {
            size: uniform.info.size,
            location: uniform.location,
            type: GLSLTypeMap[uniform.info.type],
            name: uniform.info.name
        };

        // 
        if (info.type.startsWith("sampler"))
            info.textureUnit = unit++;

        // Store Uniform Data
        uniform_data[info.name] = Object.freeze(info);
    }

    return Object.freeze(uniform_data);
}

function getUniformTree(uniforms) {

    let tree = {};

    // Construct tree from uniform data
    for (let [info, segments] of Object.values(uniforms).map(info => [info, extractSegments(info)])) {

        let uniform_ref = new Uniform(info);

        let map = tree;
        for (let i = 0; i < segments.length; i++) {
            let segment = segments[i];

            const name = segment.identifier;

            // Get the container for the id
            if (segment.access !== undefined && info.size == 1) {
                // Get/Create struct container
                if (name in map == false) map[name] = {};
                // Get/Create element container
                if (segment.access in map[name] == false) map[name][segment.access] = {};
                // Compute length
                map[name].length = Math.max(map[name].length || 1, segment.access + 1);
                // Descend into element container
                map = map[name][segment.access];
            } else {
                // Write endpoint ( single or primitive array )
                map[name] = uniform_ref;
            }
        }
    }

    // 
    return Arrayify(tree);
}

class Shader {

    /**
     * Compiles and links and new GLSL Shader Program.
     * @param {String} vert GLSL Vertex Shader Source
     * @param {String} frag GLSL Fragment Shader Source
     */
    constructor(vert, frag) {

        // Compile Shaders ( TODO: cache? )
        let vs = compileShader(vert, GL.VERTEX_SHADER);
        let fs = compileShader(frag, GL.FRAGMENT_SHADER);
        this.program = compileProgram(vs, fs);

        // Extract Shader Program Info  
        this.info = Object.freeze({
            shader: Object.freeze({ vertex: vs, fragment: fs }),
            attributes: extractAttributes(this.program),
            uniforms: extractUniforms(this.program)
        });

        // 
        this.uniforms = Object.freeze(getUniformTree(this.info.uniforms));
    }

    enable() {
        // Use the program
        // TODO: Check if already active and don't call to reduce overhead
        GL.useProgram(this.program);

        // Updates any changed uniform values
        for (let name in this.uniforms) {
            let uniform = this.uniforms[name];
            if (uniform.changed) {
                // console.log(`Shader setting uniform '${name}' to ${uniform.value}.`);
                uniform.update();
            }
        }
    }
}

/**
 * A buffer that holds GPU vertex data.
 */
// TODO: Buffer hint ( static, dynamic, stream )
class VertexBuffer {

    /**
     * Create a new vertex buffer.
     * @param {*} data Some initial data.
     */
    constructor(data = undefined) {
        this.id = GL.createBuffer();
        if (data) this.update(data);
    }

    /**
     * Update the buffer data.
     * @param {*} data An array of data to populate the buffer
     * @param {*} type Friendly name of the GL type this buffer holds
     */
    update(data, type = "float") {

        // TODO: Allow other data types
        if (type !== "float") throw new Error("Only float data allowed currently");

        // Wrap as Float32
        if (data instanceof Float32Array == false)
            data = new Float32Array(data);

        // Bind and update buffer
        GL.bindBuffer(GL.ARRAY_BUFFER, this.id);
        GL.bufferData(GL.ARRAY_BUFFER, data, GL.STATIC_DRAW);
        GL.bindBuffer(GL.ARRAY_BUFFER, null);
    }
}

/**
 * A buffer that holds GPU mesh indexing data.
 */
// TODO: Buffer hint ( static, dynamic, stream )
class IndexBuffer {

    /**
     * Create a new index buffer.
     * @param {*} data Some initial data.
     */
    constructor(data = undefined) {
        this.id = GL.createBuffer();
        if (data) this.update(data);
    }

    /**
     * Update the buffer data.
     * @param {*} data An array of data to populate the buffer
     * @param {*} type Friendly name of the GL type this buffer holds
     */
    update(data, type = "uint16") {

        // TODO: Allow other data types
        if (type !== "uint16") throw new Error("Only uint16 data allowed currently");

        // Wrap as Uint16
        if (data instanceof Uint16Array == false)
            data = new Uint16Array(data);

        // Bind and update buffer
        GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, this.id);
        GL.bufferData(GL.ELEMENT_ARRAY_BUFFER, data, GL.STATIC_DRAW);
        GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, null);
    }
}

/**
 * Default attribute layout for rendering meshes.
 */
const DefaultMeshLayout = Object.freeze([
    { type: "vec3", name: "positions" },
    { type: "vec3", name: "normals" },
    { type: "vec2", name: "uvs" }
]);

class Mesh {

    constructor(layout = DefaultMeshLayout) {
        this.layout = layout;

        let vertex_data = {};
        let element_data = {};

        let buffer_data = {
            element: {},
            vertex: {}
        };

        // Create vertex buffers
        for (var idx in this.layout) {
            var attrib = this.layout[idx].name;
            vertex_data[attrib] = [];
            buffer_data.vertex[attrib] = {
                index: +idx,
                buffer: new VertexBuffer(),
                changed: false
            };
        }

        // Create element buffers ( all primitives? )
        for (var mode of ["triangles", "lines"]) {
            element_data[mode] = [];
            buffer_data.element[mode] = {
                buffer: new IndexBuffer(),
                changed: false
            };
        }

        // Vertex Data Proxy
        this.vertices = new Proxy(vertex_data, {
            get(target, name) {
                return vertex_data[name];
            },
            set(target, name, value) {
                if (name in vertex_data) {
                    // Make falsy values an empty array
                    // TODO: Make any non-array an empty array?
                    if (!value) value = [];
                    // Mark buffer as changed, and set new data
                    buffer_data.vertex[name].changed = true;
                    vertex_data[name] = value;
                } else {
                    throw new Error(`Unable to assign vertex data to unknown attribute '${name}' in mesh.`);
                }
                return true;
            }
        });

        // Element Data Proxy
        this.elements = new Proxy(element_data, {
            get(target, name) {
                return target[name];
            },
            set(target, name, value) {

                if (name in element_data) {
                    // Make falsy values an empty array
                    // TODO: Make any non-array an empty array?
                    if (!value) value = [];
                    // Mark buffer as changed, and set new data
                    buffer_data.element[name].changed = true;
                    element_data[name] = value;
                } else {
                    throw new Error(`Unable to assign vertex data to unknown attribute '${name}' in mesh.`);
                }
                return true;
            }
        });

        this.buffers = buffer_data;
    }

    /**
     * Computes the lines for drawing a wireframe from the triangle elements.
     */
    computeWireframe() {

        let lines = [];

        const triangles = this.elements.triangles;
        for (let i = 0; i < triangles.length; i++) {
            lines.push(triangles[i]);
            lines.push(triangles[(i + 1) % triangles.length]);
        }

        this.elements.lines = lines;
    }

    updateBuffers() {
        for (let mode in this.elements) {
            this.elements[mode];
        }
    }

    draw(shader, mode = "triangles") {

        // Select primitive mode
        let primitive = undefined;
        switch (mode) {
            case "triangles": primitive = GL.TRIANGLES; break;
            case "lines": primitive = GL.LINES; break;
        }

        // 
        if (primitive === undefined) throw new Error("Unable to draw mesh, invalid mode.");
        else {

            const buffers = this.buffers;

            // Configure Attributes 
            for (let name in buffers.vertex) {
                let vItem = buffers.vertex[name];

                // If needed, update buffer.
                if (vItem.changed) {
                    // console.log(`Updating Vertices: ${name}`);
                    vItem.buffer.update(this.vertices[name]);
                    vItem.changed = false;
                }

                // 
                let size = GLSLTypeSize[this.layout[vItem.index].type];

                // 
                // Bind vertex buffer
                GL.bindBuffer(GL.ARRAY_BUFFER, vItem.buffer.id);
                GL.vertexAttribPointer(vItem.index, size, GL.FLOAT, false, 0, 0);
                GL.enableVertexAttribArray(vItem.index);
            }

            // Bind Element Buffer 
            let elementBuffer = buffers.element[mode];
            if (elementBuffer.changed) {
                elementBuffer.buffer.update(this.elements[mode]);
                elementBuffer.changed = false;
            }

            // Bind vertex buffer
            GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, elementBuffer.buffer.id);

            // Enable shader 
            shader.enable();

            // Draw Mesh
            GL.drawElements(primitive, this.elements[mode].length, GL.UNSIGNED_SHORT, 0);

            // Unbind buffers 
            GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, null);
            GL.bindBuffer(GL.ARRAY_BUFFER, null);
        }
    }

    /** 
     * Constructs a simple quad mesh, oriented on the xy-plane.
     */
    static CreateSimpleQuad() {

        // Create a simple quad mesh
        var mesh = new Mesh();
        mesh.vertices.positions = [-1.0, -1.0, 0.0, +1.0, -1.0, 0.0, +1.0, +1.0, 0.0, -1.0, +1.0, 0.0,];
        mesh.vertices.normals = [0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0];
        mesh.vertices.uvs = [0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0];
        mesh.elements.triangles = [0, 1, 2, 0, 2, 3];
        mesh.computeWireframe();

        return mesh;
    }

    /**
     * Creates a sphere
     */
    static CreateSphere(stacks = 6, slices = 12) {

        let positions = [];
        let normals = [];
        let uvs = [];

        let triangles = [];

        const inc = Math.PI / 4;

        const cos = Math.cos, sin = Math.sin;
        const append = (array, values) => Array.prototype.push.apply(array, values);

        // 
        for (let p1 = 0; p1 < slices; p1++) {
            let pA = (p1 / slices) * 2.0 * Math.PI;

            // 
            for (let t1 = 0; t1 < stacks; t1++) {
                let tA = (t1 / stacks) * Math.PI;

                //
                let x = sin(tA) * cos(pA);
                let y = sin(tA) * sin(pA);
                let z = cos(tA);

                // Append Vertex Set
                append(uvs, [p1 / slices, t1 / stacks]);
                append(positions, [x, y, z]); // * radius
                append(normals, [x, y, z]);

                // Map Triangles
                let t2 = t1 === (stacks - 1) ? 0 : t1 + 1;
                let p2 = p1 === (slices - 1) ? 0 : p1 + 1;

                let idx0 = p1 * stacks + t1;
                let idx1 = p2 * stacks + t1;
                let idx2 = p1 * stacks + t2;
                let idx3 = p2 * stacks + t2;

                // Append Quad
                append(triangles, [idx0, idx1, idx2]);
                append(triangles, [idx2, idx1, idx3]);
            }
        }

        // Create a simple quad mesh
        var mesh = new Mesh();
        mesh.elements.triangles = triangles;
        mesh.vertices.positions = positions;
        mesh.vertices.normals = normals;
        mesh.vertices.uvs = uvs;
        mesh.computeWireframe();

        return mesh;
    }
}

var TextureLoader = {

    Load(path, progress) {

        let load = Loader.Get(path, "blob", progress);

        return new Promise((resolve, reject) => {

            load.then(blob => {

                // Load image into browser context
                let image = new Image();
                image.src = window.URL.createObjectURL(blob);
                image.onload = () => {
                    // Success, we have a texture.
                    resolve(new Texture(image));
                };
            });

            // Failure
            load.catch(reject);
        });
    }
};

function constructOpenGLMeshes(model) {

    var data = [];

    // For each OBJ Object
    model.objects.forEach(obj => {

        var triangles = [];
        var positions = [];
        var normals = [];
        var uvs = [];

        let index = 0;

        // Each face in object
        for (var face of obj.faces) {

            // Each vertex in face
            for (var f of face) {
                // 
                Array.prototype.push.apply(positions, model.positions[f.v - 1]);
                Array.prototype.push.apply(normals, model.normals[f.n - 1]);
                Array.prototype.push.apply(uvs, model.uvs[f.t - 1]);

                // 
                triangles.push(index++);
            }
        }

        var mesh = new Mesh();

        //
        mesh.vertices.positions = positions;
        mesh.vertices.normals = normals;
        mesh.vertices.uvs = uvs;

        //
        mesh.elements.triangles = triangles;
        mesh.computeWireframe();

        data.push({
            name: obj.name,
            material: obj.material,
            mesh: mesh
        });

    }, this);

    return data;
}

var ObjLoader = {

    Load(path, progress) {

        let load = Loader.Get(path, "text", progress);

        return new Promise((resolve, reject) => {

            load.then(text => {

                let current = undefined;

                let model = {
                    positions: [],
                    normals: [],
                    uvs: [],
                    library: undefined,
                    objects: []
                };

                let lineIndex = 0;
                let lines = text.split('\n');
                while (lineIndex < lines.length) {
                    let line = lines[lineIndex++].trim();

                    // Skip comments
                    if (line.startsWith("#"))
                        continue;

                    // 
                    let segments = line.split(' ');

                    switch (segments[0]) {

                        case "mtllib":
                            model.library = segments[1];
                            break;

                        case "usemtl":
                            current.material = segments[1];
                            break;

                        case "o":
                            model.objects.push(current = {
                                name: segments[1],
                                faces: []
                            });
                            break;

                        case "v":
                            //
                            var x = parseFloat(segments[1]);
                            var y = parseFloat(segments[2]);
                            var z = parseFloat(segments[3]);

                            // Vertex position definition 
                            model.positions.push([x, y, z]);
                            break;

                        case "vn":
                            //
                            var x = parseFloat(segments[1]);
                            var y = parseFloat(segments[2]);
                            var z = parseFloat(segments[3]);

                            // Vertex normal definition
                            model.normals.push([x, y, z]);
                            break;

                        case "vt":
                            //
                            var x = parseFloat(segments[1]);
                            var y = parseFloat(segments[2]);

                            // Vertex uv definition
                            model.uvs.push([x, y]);
                            break;

                        case "f":
                            let face = [];
                            for (var i = 1; i < segments.length; i++) {
                                var indices = segments[i].split('/');
                                face.push({
                                    v: +indices[0],
                                    t: +indices[1],
                                    n: +indices[2]
                                });
                            }

                            // Triangulate
                            for (var i = 0; i < face.length - 2; i++)
                                current.faces.push([face[0], face[i + 1], face[i + 2]]);

                            break;
                    }
                }

                // 
                let data = constructOpenGLMeshes(model);
                resolve(data);
            });

            // Failure
            load.catch(reject);
        });
    }
};

let CustomHandlers = {
    // Wavefront OBJ Moels
    "obj": ObjLoader,
    // Textures via HTML Images
    "jpg": TextureLoader,
    "png": TextureLoader,
    "bmp": TextureLoader,
    "gif": TextureLoader,
};

// Append loading indicator element
let progressElement = document.createElement("div");
progressElement.classList = "progress";
document.body.appendChild(progressElement);

/**
 * Displays a loading indicator.
 * @param {String} text The text to display
 */
function ShowProgress(text) {
    progressElement.style.display = "block";
    progressElement.textContent = text || "";
}

/**
 * Hides the loading indicator.
 */
function HideProgress() {
    progressElement.style.display = "none";
}

/**
 * Registers a custom loading handler for a specific file extension.
 * @param {String} ext File extension ( ex. 'png' )
 * @param {Object} handler An object with a load member
 */
function RegisterCustomLoadHandler(ext, handler) {
    CustomHandlers[ext] = handler;
}

/**
 * Asynchronously loads a remote asset.
 * @param {String} url A standard HTTP GET request.
 * @param {String} type Response type.
 * @param {Function} progress Optional progress reporting function
 */
function Get(url, type = "text", progress = undefined) {
    return new Promise((resolve, reject) => {

        var request = new XMLHttpRequest();
        request.responseType = type || "text";

        if (progress) {
            request.onprogress = ev => {
                if (ev.lengthComputable) progress(ev.loaded / ev.total);
                else progress(0.0); // No length...?
            };
        }

        request.onerror = () => reject({ status: response.status, message: response.statusText });
        request.onload = () => resolve(request.response);

        request.open('GET', url, true);
        request.send(null);
    });
}

/**
 * Loads an asset, and possibly processes it with a custom loading script.
 * 
 * @param {String} url Some URL to a remote asset.
 * @param {String} type Some type to use ( null for default )
 * @param {Function} progress Optional function for reporting load progress.
 */
function LoadAsset(url, type, progress) {
    let ext = url.split('.').pop();
    if (ext in CustomHandlers) return CustomHandlers[ext].Load(url, progress);
    else return Get(url, type, progress);
}

/**
 * Loads an hierachical set of assets.
 * @param {String|Object} manifest The path to a JSON file or an object describing asset heirarchy to load.
 */
function LoadManifest(manifest) {

    const loadWithManifest = manifest => {

        ShowProgress("Loading");

        // 
        return new Promise((resolve, reject) => {

            // 
            let total = 0, current = 0;
            let promises = [];

            // Load a single asset
            const load = (data, key, url) => {

                // When the asset is loaded
                let n = promises.push(LoadAsset(url, false, () => {
                    let progress = Math.floor((current / total) * 100);
                    ShowProgress(progress + "%");
                }));

                total++; // 
                promises[n - 1].then(value => {
                    data[key] = value;
                    current++;
                });
            };

            // Recursive loading
            const rec = (data) => {

                // For each entry in object
                for (let key in data) {
                    let item = data[key];
                    let type = getTypeOf(item);

                    // Object ( Containers )
                    if (type === "object") rec(item);
                    // Items ( Assets )
                    else if (type === "string") {
                        load(data, key, item);
                    } else if (type === "array") {

                        for (var i = 0; i < item.length; i++)
                            load(item, i, item[i]);

                    }
                }
            };

            // 
            rec(manifest);

            // When everything is loaded
            Promise.all(promises).then(() => {
                resolve(manifest);
                HideProgress();
            });
        });
    };

    // 
    return new Promise((resolve, reject) => {

        // Loading from JSON from url
        if (typeof manifest === "string") {

            // Loads the JSON
            Get(manifest, "text")
                .catch(error => reject)
                .then(text => {
                    // Load the manifest then resolve promise
                    loadWithManifest(JSON.parse(text))
                        .catch(error => reject)
                        .then(resolve);
                });
        }
        // Loading from manifest directly
        else if (typeof manifest === "object" && !Array.isArray(manifest)) {
            // Load, then resolve promise
            loadWithManifest(manifest)
                .catch(error => reject)
                .then(resolve);
        } else {
            // Don't know what to do with this.
            reject(`Unable to load assets from '${typeof manifest}'.`);
        }
    });
}

var Loader = {
    Get, LoadManifest,
    RegisterCustomLoadHandler,
    ShowProgress, HideProgress
};

// Clamp the list positions to allow looping
function wrapList(pos, size) {

    while (pos < 0)
        pos += size;


    while (pos >= size)
        pos -= size;

    return pos;
}


//Returns a position between 4 Vector3 with Catmull-Rom spline algorithm
//http://www.iquilezles.org/www/articles/minispline/minispline.htm
function getCatmullRomPosition(t, p0, p1, p2, p3) {
    // The coefficients of the cubic polynomial (except the 0.5f * which I added later for performance)
    let a = 2 * p1;
    let b = p2 - p0;
    let c = 2 * p0 - 5 * p1 + 4 * p2 - p3;
    let d = -p0 + 3 * p1 - 3 * p2 + p3;

    // The cubic polynomial: a + b * t + c * t^2 + d * t^3
    return 0.5 * (a + (b * t) + (c * t * t) + (d * t * t * t));
}

// http://www.habrador.com/tutorials/interpolation/1-catmull-rom-splines/
class CatmullRomSpline {

    constructor() {
        this._points = [];
    }

    /**
     * Append a control point on the curve.
     * @param {Number} value Some x-coordinate value.
     */
    addPoint(value) {
        this._points.push(value);
    }

    /**
     * Evaluate an interpolated value along the curve.
     * @param {Number} time A 0 - n value where n is the number of points in the curve.
     */
    eval(time) {

        let frame = Math.floor(time);
        let t = time - frame;

        // The 4 points we need to form a spline between p1 and p2
        let p0 = this._points[wrapList(frame - 1, this._points.length)];
        let p1 = this._points[wrapList(frame + 0, this._points.length)];
        let p2 = this._points[wrapList(frame + 1, this._points.length)];
        let p3 = this._points[wrapList(frame + 2, this._points.length)];

        // 
        return getCatmullRomPosition(t, p0, p1, p2, p3);
    }
}

// Cached matrix to prevent allocation every frame
// let mRotate = Matrix.identity();
// let mTranslate = Matrix.identity();
// let mScale = Matrix.identity();

/**
 * An object for managing the transform of an model.
 */
class Transform {

    constructor(position, rotation, scale) {
        // 
        this.position = position || new Vector();
        this.rotation = rotation || Quaternion.identity();
        this.scale = scale || new Vector(1, 1, 1);
        // 
        this.matrix = new Matrix();
    }

    update() {
        // Compute Transform Components
        let mTranslate = Matrix.translate(this.position.x, this.position.y, this.position.z);
        let mScale = Matrix.scale(this.scale.x, this.scale.y, this.scale.z);
        let mRotate = Matrix.fromQuaternion(this.rotation);

        // Merge ( T * R * S )
        this.matrix = mScale.multiply(mRotate.multiply(mTranslate));
    }
}

class Material {

    constructor(shader, texture) {
        this.texture = texture;
        this.shader = shader;
        this.color = [1.0, 1.0, 1.0];
    }

    apply() {
        this.shader.enable();
        this.shader.uniforms.Albedo.set(this.texture);
        this.shader.uniforms.Blend.set(this.color);
    }
}

class Model {

    constructor(meshes, material, transform) {
        this.meshes = EnsureArray(meshes);
        this.transform = transform;
        this.material = material;
    }

    get shader() {
        return this.material.shader;
    }

    draw(transform) {
        // 
        this.material.apply();

        // Assign model transform
        this.transform.update(); // Recompute transform
        this.shader.uniforms.Transform.set(this.transform.matrix);

        // Draw associated meshes
        for (let submesh of this.meshes)
            submesh.draw(this.shader);
    }
}

const rand = (min, max) => min + random() * (max - min);
const randi = (min, max) => rand(min, max) | 0;

document.addEventListener("DOMContentLoaded", () => {

    /** Set the canvas and make "fullscreen" */
    Context.SetCanvas("#canvas");     // Creates the context for the canvas element
    Context.SetFullscreen("#canvas"); // Responds to resize events

    const X_CORRECTION = Quaternion.fromAxisAngle(Vector.UnitX, 3.14 / 2);

    // 
    Loader.LoadManifest({
        "shaders": {
            "basic": {
                "vert": "./assets/shaders/shader.vert",
                "frag": "./assets/shaders/shader.frag",
            }
        },
        "skybox": {
            "model": "./assets/skybox.obj",
            "texture": "./assets/skybox.jpg"
        },
        "textures": {
            "car_light": "./assets/car_light.jpg",
            "building": "./assets/windows2.jpg"
        },
        "buildings": [
            "./assets/building_01.obj",
            "./assets/building_02.obj",
            "./assets/building_03.obj",
            "./assets/building_04.obj",
            "./assets/building_05.obj",
            "./assets/building_06.obj",
            "./assets/building_07.obj",
            "./assets/building_08.obj",
            "./assets/building_09.obj",
            "./assets/building_10.obj"
        ]
    }).then(assets => {

        // 
        setRandomSeed(1511822366614);

        // Log assets
        console.log(assets);

        // Create Procedural Mesh
        const sphereMesh = Mesh.CreateSphere();
        const quadMesh = Mesh.CreateSimpleQuad();

        // Compile basic shader
        const shader = new Shader(
            assets.shaders.basic.vert,
            assets.shaders.basic.frag
        );

        // Create building material
        const building_material = new Material(shader, assets.textures.building);

        // Construct city
        const city = new City((pos, rot) => {

            // Randomly select a building asset
            let building_index = randi(0, assets.buildings.length);
            let model_asset = assets.buildings[building_index][0];

            // Append model to listing
            return new Model(model_asset.mesh, building_material, new Transform(pos, rot));
        });

        // Create ground plane
        // TODO: Ground material?
        const groundModel = new Model(quadMesh, building_material, new Transform(null, null, new Vector(1000, 1000, 0.01)));

        // Create skybox 
        const skyModel = new Model(assets.skybox.model[0].mesh,
            new Material(shader, assets.skybox.texture),
            new Transform(null, null, new Vector(768, 768, 768)));


        // 
        let cameraPath = new CameraPath();
        cameraPath.addPoint(-city.extent / 2, -city.extent / 2, 7.0);
        cameraPath.addPoint(+city.extent / 2, -city.extent / 2, 11.0);
        cameraPath.addPoint(+city.extent / 2, +city.extent / 2, 7.5);
        cameraPath.addPoint(-city.extent / 2, +city.extent / 2, 14.0);
        cameraPath.addPoint(-city.extent / 4, +city.extent / 4, 10.0);
        cameraPath.addPoint(+city.extent / 4, +city.extent / 4, 5.5);
        cameraPath.addPoint(+city.extent / 4, -city.extent / 4, 12.0);
        cameraPath.addPoint(-city.extent / 4, -city.extent / 4, 7.0);

        // Populate city with flying cars
        let cars = [];
        const car_colors = [[1.0, 0.9, 0.7], [0.7, 0.9, 1.0]];
        for (let c = 0; c < 250; c++) {

            // Selects a random intersection
            let x = (rand(-city.size, +city.size) | 0) * city.spacing + city.spacing / 2;
            let y = (rand(-city.size, +city.size) | 0) * city.spacing + city.spacing / 2;
            let z = rand(0.15, 6.5);

            // Randomize within intersection
            x += rand(-city.roadWidth, +city.roadWidth) * 0.9;
            y += rand(-city.roadWidth, +city.roadWidth) * 0.9;

            // Randomly chooses attributes of cars
            let r_scale = rand(0.25, 1.25);
            let car_scale = new Vector(0.1 * r_scale, 0.025 * r_scale, 0.025 * r_scale);
            let car_position = new Vector(x, y, z);
            let car_angle = ((random() * 4) | 0) * Math.PI / 2;
            let car_rotation = Quaternion.fromAxisAngle(Vector.UnitZ, car_angle);

            // Creates a material representing the car
            let car_light_material = new Material(shader, assets.textures.car_light);
            car_light_material.color = car_colors[randi(0, car_colors.length)];

            // Appends the car to the car models list
            let model = new Model(sphereMesh, car_light_material, new Transform(car_position, car_rotation, car_scale));
            cars.push(model);
        }

        let time = 0.0;

        // Render Loop
        Context.SetRenderCallback(() => {

            // Clear Framebuffer
            GL.clearColor(0.15, 0.12, 0.09, 1.0);
            GL.clear(GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT);

            // Set rendering region to the full framebuffer
            GL.viewport(0, 0, Canvas.width, Canvas.height);

            // == Update/Animate Cars ==
            for (let car of cars) {

                // Compute motion vector ( TODO: Precompute when creating cars )
                let m = Matrix.fromQuaternion(car.transform.rotation);
                let v = m.transformVector(Vector.UnitX).multiply(0.06);

                // Animate cars
                car.transform.position = car.transform.position.add(v);

                // Wrap cars around city bound
                if (car.transform.position.x > +city.extent) car.transform.position.x = -city.extent;
                if (car.transform.position.y > +city.extent) car.transform.position.y = -city.extent;
                if (car.transform.position.x < -city.extent) car.transform.position.x = +city.extent;
                if (car.transform.position.y < -city.extent) car.transform.position.y = +city.extent;
            }

            time += 1 / 666;

            // == Compute Camera == //
            
            const cameraPosition = cameraPath.eval(time + 0);
            const cameraLookAt = cameraPath.eval(time + 1.1);

            // cameraPosition.set(0, 0.00001, 35);

            // Compute camera matrix
            let projMatrix = Matrix.perspective(75 * Math.PI / 180, Canvas.width / Canvas.height, 0.01, 1000);
            let viewMatrix = Matrix.lookAt(cameraPosition, cameraLookAt, Vector.UnitZ);
            let cameraMatrix = viewMatrix.multiply(projMatrix);

            // Assign camera uniforms
            // TODO: Make implicit in a render pipeline?
            shader.uniforms.Camera.set(cameraMatrix);
            shader.uniforms.CameraPos.set(cameraPosition);
            shader.uniforms.Skybox.set(assets.skybox.texture);

            // == Draw == 

            // Draw Sky
            skyModel.draw();

            // Draw Ground
            groundModel.draw();

            // Draw Buildings
            // TODO: Could static batch
            for (let building of city.buildings)
                building.draw();

            // Draw Cars
            // TODO: Could instance batch
            for (let model of cars)
                model.draw();
        });
    });

    class CameraPath {

        /**
         * 
         */
        constructor() {
            this.curveX = new CatmullRomSpline();
            this.curveY = new CatmullRomSpline();
            this.curveZ = new CatmullRomSpline();
        }

        get length() {
            return this.curveX._points.length;
        }

        addPoint(x, y, z) {
            this.curveX.addPoint(x);
            this.curveY.addPoint(y);
            this.curveZ.addPoint(z);
        }

        eval(time, result = undefined) {
            result = result || new Vector();

            const x = this.curveX.eval(time);
            const y = this.curveY.eval(time);
            const z = this.curveZ.eval(time);
            result.set(x, y, z);

            return result;
        }
    }

    class City {

        constructor(populateCallback) {

            // City parameters
            this.size = 7;
            this.spacing = 3.5;

            // 
            this.roadWidth = Math.max(1.0, this.spacing - 1.5) / 2;
            this.extent = this.spacing * this.size;

            this.buildings = [];

            const PI_4 = Math.PI / 4;

            // Populate city with buildings
            for (let x = -this.size; x <= this.size; x++) {
                for (let y = -this.size; y <= this.size; y++) {

                    // Grid aligned and randomly rotate buildings ( 8 directional )
                    let rotation = Quaternion.fromAxisAngle(Vector.UnitZ, randi(0, 8) * PI_4);
                    let position = new Vector(x * this.spacing, y * this.spacing, rand(-1, 0));

                    // 
                    let building = populateCallback(position, rotation);
                    this.buildings.push(building);
                }
            }
        }
    }
});
