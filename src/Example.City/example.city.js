import { Context, Canvas, GL } from "../Engine/Context.js";
import Loader from "../Engine/Loader.js";
import Shader from "../Engine/Shader.js";
import { Mesh } from '../Engine/Mesh.js';

import CatmullRomSpline from "../Engine/CatmullRomSpline.js";
import { random, setRandomSeed, getRandomSeed } from "../Engine/Helpers.js";
import { Matrix, Vector, Quaternion } from '../Engine/Math.js';
import Transform from "../Engine/Transform.js";

import Material from "./Material.js";
import Model from "./Model.js";
import Camera from "./Camera.js";

const rand = (min, max) => min + random() * (max - min);
const randi = (min, max) => rand(min, max) | 0;

document.addEventListener("DOMContentLoaded", () => {

    /** Set the canvas and make "fullscreen" */
    Context.SetCanvas("#canvas");     // Creates the context for the canvas element
    Context.SetFullscreen("#canvas"); // Responds to resize events

    const X_CORRECTION = Quaternion.fromAxisAngle(Vector.UnitX, 3.14 / 2);

    // 
    Loader.LoadManifest({
        "shaders": {
            "basic": {
                "vert": "./assets/shaders/shader.vert",
                "frag": "./assets/shaders/shader.frag",
            }
        },
        "skybox": {
            "model": "./assets/skybox.obj",
            "texture": "./assets/skybox.jpg"
        },
        "textures": {
            "car_light": "./assets/car_light.jpg",
            "building": "./assets/windows2.jpg"
        },
        "buildings": [
            "./assets/building_01.obj",
            "./assets/building_02.obj",
            "./assets/building_03.obj",
            "./assets/building_04.obj",
            "./assets/building_05.obj",
            "./assets/building_06.obj",
            "./assets/building_07.obj",
            "./assets/building_08.obj",
            "./assets/building_09.obj",
            "./assets/building_10.obj"
        ]
    }).then(assets => {

        // 
        setRandomSeed(1511822366614);

        // Log assets
        console.log(assets);

        // Create Procedural Mesh
        const sphereMesh = Mesh.CreateSphere();
        const quadMesh = Mesh.CreateSimpleQuad();

        // Compile basic shader
        const shader = new Shader(
            assets.shaders.basic.vert,
            assets.shaders.basic.frag
        );

        // Create building material
        const building_material = new Material(shader, assets.textures.building);

        // Construct city
        const city = new City((pos, rot) => {

            // Randomly select a building asset
            let building_index = randi(0, assets.buildings.length);
            let model_asset = assets.buildings[building_index][0];

            // Append model to listing
            return new Model(model_asset.mesh, building_material, new Transform(pos, rot));
        });

        // Create ground plane
        // TODO: Ground material?
        const groundModel = new Model(quadMesh, building_material, new Transform(null, null, new Vector(1000, 1000, 0.01)));

        // Create skybox 
        const skyModel = new Model(assets.skybox.model[0].mesh,
            new Material(shader, assets.skybox.texture),
            new Transform(null, null, new Vector(768, 768, 768)));

        // 
        let cameraPath = new CameraPath();
        cameraPath.addPoint(-city.extent / 2, -city.extent / 2, 7.0);
        cameraPath.addPoint(+city.extent / 2, -city.extent / 2, 11.0);
        cameraPath.addPoint(+city.extent / 2, +city.extent / 2, 7.5);
        cameraPath.addPoint(-city.extent / 2, +city.extent / 2, 14.0);
        cameraPath.addPoint(-city.extent / 4, +city.extent / 4, 10.0);
        cameraPath.addPoint(+city.extent / 4, +city.extent / 4, 5.5);
        cameraPath.addPoint(+city.extent / 4, -city.extent / 4, 12.0);
        cameraPath.addPoint(-city.extent / 4, -city.extent / 4, 7.0);

        // Populate city with flying cars
        let cars = [];
        const car_colors = [[1.0, 0.9, 0.7], [0.7, 0.9, 1.0]];
        for (let c = 0; c < 250; c++) {

            // Selects a random intersection
            let x = (rand(-city.size, +city.size) | 0) * city.spacing + city.spacing / 2;
            let y = (rand(-city.size, +city.size) | 0) * city.spacing + city.spacing / 2;
            let z = rand(0.15, 6.5);

            // Randomize within intersection
            x += rand(-city.roadWidth, +city.roadWidth) * 0.9;
            y += rand(-city.roadWidth, +city.roadWidth) * 0.9;

            // Randomly chooses attributes of cars
            let r_scale = rand(0.25, 1.25);
            let car_scale = new Vector(0.1 * r_scale, 0.025 * r_scale, 0.025 * r_scale);
            let car_position = new Vector(x, y, z);
            let car_angle = ((random() * 4) | 0) * Math.PI / 2;
            let car_rotation = Quaternion.fromAxisAngle(Vector.UnitZ, car_angle);

            // Creates a material representing the car
            let car_light_material = new Material(shader, assets.textures.car_light);
            car_light_material.color = car_colors[randi(0, car_colors.length)];

            // Appends the car to the car models list
            let model = new Model(sphereMesh, car_light_material, new Transform(car_position, car_rotation, car_scale));
            cars.push(model);
        }

        let time = 0.0;

        // Render Loop
        Context.SetRenderCallback(() => {

            // Clear Framebuffer
            GL.clearColor(0.15, 0.12, 0.09, 1.0);
            GL.clear(GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT);

            // Set rendering region to the full framebuffer
            GL.viewport(0, 0, Canvas.width, Canvas.height);

            // == Update/Animate Cars ==
            for (let car of cars) {

                // Compute motion vector ( TODO: Precompute when creating cars )
                let m = Matrix.fromQuaternion(car.transform.rotation);
                let v = m.transformVector(Vector.UnitX).multiply(0.06);

                // Animate cars
                car.transform.position = car.transform.position.add(v);

                // Wrap cars around city bound
                if (car.transform.position.x > +city.extent) car.transform.position.x = -city.extent;
                if (car.transform.position.y > +city.extent) car.transform.position.y = -city.extent;
                if (car.transform.position.x < -city.extent) car.transform.position.x = +city.extent;
                if (car.transform.position.y < -city.extent) car.transform.position.y = +city.extent;
            }

            time += 1 / 666;

            // == Compute Camera == //

            const cameraPosition = cameraPath.eval(time + 0);
            const cameraLookAt = cameraPath.eval(time + 1.1);

            // cameraPosition.set(0, 0.00001, 35);

            // Compute camera matrix
            let projMatrix = Matrix.perspective(75 * Math.PI / 180, Canvas.width / Canvas.height, 0.01, 1000);
            let viewMatrix = Matrix.lookAt(cameraPosition, cameraLookAt, Vector.UnitZ);
            let cameraMatrix = viewMatrix.multiply(projMatrix);

            // Assign camera uniforms
            // TODO: Make implicit in a render pipeline?
            shader.uniforms.Camera.set(cameraMatrix);
            shader.uniforms.CameraPos.set(cameraPosition);
            shader.uniforms.Skybox.set(assets.skybox.texture);

            // == Draw == 

            // Draw Sky
            skyModel.draw();

            // Draw Ground
            groundModel.draw();

            // Draw Buildings
            // TODO: Could static batch
            for (let building of city.buildings)
                building.draw();

            // Draw Cars
            // TODO: Could instance batch
            for (let model of cars)
                model.draw();
        });
    });

    class CameraPath {

        /**
         * 
         */
        constructor() {
            this.curveX = new CatmullRomSpline();
            this.curveY = new CatmullRomSpline();
            this.curveZ = new CatmullRomSpline();
        }

        get length() {
            return this.curveX._points.length;
        }

        addPoint(x, y, z) {
            this.curveX.addPoint(x);
            this.curveY.addPoint(y);
            this.curveZ.addPoint(z);
        }

        eval(time, result = undefined) {
            result = result || new Vector();

            const x = this.curveX.eval(time);
            const y = this.curveY.eval(time);
            const z = this.curveZ.eval(time);
            result.set(x, y, z);

            return result;
        }
    }

    class City {

        constructor(populateCallback) {

            // City parameters
            this.size = 7;
            this.spacing = 3.5;

            // 
            this.roadWidth = Math.max(1.0, this.spacing - 1.5) / 2;
            this.extent = this.spacing * this.size;

            this.buildings = [];

            const PI_4 = Math.PI / 4;

            // Populate city with buildings
            for (let x = -this.size; x <= this.size; x++) {
                for (let y = -this.size; y <= this.size; y++) {

                    // Grid aligned and randomly rotate buildings ( 8 directional )
                    let rotation = Quaternion.fromAxisAngle(Vector.UnitZ, randi(0, 8) * PI_4);
                    let position = new Vector(x * this.spacing, y * this.spacing, rand(-1, 0));

                    // 
                    let building = populateCallback(position, rotation);
                    if (building) this.buildings.push(building);
                }
            }
        }
    }
});