import { GL } from "./Context.js";

/**
 * A buffer that holds GPU mesh indexing data.
 */
// TODO: Buffer hint ( static, dynamic, stream )
export default class IndexBuffer {

    /**
     * Create a new index buffer.
     * @param {*} data Some initial data.
     */
    constructor(data = undefined) {
        this.id = GL.createBuffer();
        if (data) this.update(data);
    }

    /**
     * Update the buffer data.
     * @param {*} data An array of data to populate the buffer
     * @param {*} type Friendly name of the GL type this buffer holds
     */
    update(data, type = "uint16") {

        // TODO: Allow other data types
        if (type !== "uint16") throw new Error("Only uint16 data allowed currently");

        // Wrap as Uint16
        if (data instanceof Uint16Array == false)
            data = new Uint16Array(data);

        // Bind and update buffer
        GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, this.id);
        GL.bufferData(GL.ELEMENT_ARRAY_BUFFER, data, GL.STATIC_DRAW);
        GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, null);
    }
}