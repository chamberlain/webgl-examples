import { Matrix, Vector, Quaternion } from '../Engine/Math.js';

// Cached matrix to prevent allocation every frame
// let mRotate = Matrix.identity();
// let mTranslate = Matrix.identity();
// let mScale = Matrix.identity();

/**
 * An object for managing the transform of an model.
 */
export default class Transform {

    constructor(position, rotation, scale) {
        // 
        this.position = position || new Vector();
        this.rotation = rotation || Quaternion.identity();
        this.scale = scale || new Vector(1, 1, 1);
        // 
        this.matrix = new Matrix();
    }

    update() {
        // Compute Transform Components
        let mTranslate = Matrix.translate(this.position.x, this.position.y, this.position.z);
        let mScale = Matrix.scale(this.scale.x, this.scale.y, this.scale.z);
        let mRotate = Matrix.fromQuaternion(this.rotation);

        // Merge ( T * R * S )
        this.matrix = mScale.multiply(mRotate.multiply(mTranslate));
    }
}