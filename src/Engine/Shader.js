import { GL } from "./Context.js";
import { Matrix, Vector, Quaternion } from '../Engine/Math.js';
import { Arrayify } from './Helpers.js';
import Uniform from "./Uniform.js";

/** Maps GL constants with friendly GLSL types. */
export const GLSLTypeMap = {
    5124: "int", 5126: "float", 35670: "bool",
    35664: "vec2", 35665: "vec3", 35666: "vec4",
    35667: "ivec2", 35668: "ivec3", 35669: "ivec4",
    35671: "bvec2", 35672: "bvec3", 35673: "bvec4",
    35674: "mat2", 35675: "mat3", 35676: "mat4",
    35678: "sampler2D", 35680: "samplerCube"
};

// Make bi-directional mapping of the types
for (let k in GLSLTypeMap)
    GLSLTypeMap[GLSLTypeMap[k]] = +k;

/** Maps friendly GLSL types to their respective component count. */
export const GLSLTypeSize = {
    "int": 1, "float": 1, "bool": 1,
    "vec2": 2, "vec3": 3, "vec4": 4,
    "ivec2": 2, "ivec3": 3, "ivec4": 4,
    "bvec2": 2, "bvec3": 3, "bvec4": 4,
    "sampler2D": 1, "samplerCube": 1
};

/** Maps friendly GLSL types to their primitive base type. */
export const GLSLTypeBase = {
    "int": "int", "float": "float", "bool": "bool",
    "vec2": "float", "vec3": "float", "vec4": "float",
    "ivec2": "int", "ivec3": "int", "ivec4": "int",
    "bvec2": "bool", "bvec3": "bool", "bvec4": "bool",
    "sampler2D": "int", "samplerCube": "int"
};

/**
 * Compiles a GLSL Shader.
 * @param {String} sourceCode GLSL ES Source Code
 * @param {Number} type GL Constant ( Either FRAGMENT_SHADER or VERTEX_SHADER )
 */
function compileShader(sourceCode, type) {

    // Create shader
    var shader = GL.createShader(type);

    // Set and compile shader source
    GL.shaderSource(shader, sourceCode);
    GL.compileShader(shader);

    // Check compile status
    if (!GL.getShaderParameter(shader, GL.COMPILE_STATUS)) {
        var info = GL.getShaderInfoLog(shader);
        GL.deleteShader(shader); // Remove shader, didn't compile.
        throw "Could not compile GLSL Shader. \n\n" + info;
    }

    // 
    return shader;
}

/**
 * Links the given shaders together into a single program.
 * @param {Number} vertexShader Handle to GLSL Vertex Shader
 * @param {Number} fragmentShader Handle to GLSL Fragment Shader
 */
function compileProgram(vertexShader, fragmentShader) {

    // Link Programs
    let program = GL.createProgram();

    // Attach pre-existing shaders
    GL.attachShader(program, vertexShader);
    GL.attachShader(program, fragmentShader);
    GL.linkProgram(program);

    // Check compile status
    if (!GL.getProgramParameter(program, GL.LINK_STATUS)) {
        var info = GL.getProgramInfoLog(program);
        GL.deleteProgram(program); // Remove program, didn't link.
        throw "Could not compile GLSL Program. \n\n" + info;
    }

    return program;
}

/**
 * Extracts the segments of the uniform path.
 * ie. Splits "A.B[2].C" to [{A}, {B,2}, {C}]
 */
function extractSegments(info) {

    let path = info.name;

    let segments = [];
    while (path.length > 0) {

        // 
        let structPos = path.indexOf(".");

        // 
        let identifier = structPos >= 0 ? path.substr(0, structPos) : path;
        let access = undefined;

        // BUG: 'A.B[0].C' doesn't split properly.

        // 
        let arrayPos = path.indexOf("[");
        if (arrayPos >= 0) {
            // 
            let endArrayPos = identifier.indexOf("]");
            access = +identifier.substr(arrayPos + 1, endArrayPos - arrayPos - 1);

            // 
            identifier = identifier.substr(0, arrayPos);

            // Chop off end of path
            if (structPos >= 0) path = path.substr(structPos + 1);
            else path = path.substr(endArrayPos + 1);
        } else {
            // 
            name = path;
            // 
            if (structPos >= 0) path = path.substr(structPos + 1);
            else path = "";
        }

        // 
        let segment = { identifier };
        if (access !== undefined)
            segment.access = access;

        // 
        segments.push(segment);
    }

    return segments;
}

function extractFromProgram(program, count, getInfo, getLocation) {

    let attributes = [];

    // Enumerate uniforms and store information about each
    for (var index = 0; index < count; index++) {

        let info = getInfo(program, index);

        // Get information for the i-th uniform
        attributes.push({
            location: getLocation(program, info.name),
            info
        })
    }

    return attributes;
}

function extractAttributes(program) {

    // Enumerate attributes and store information about each 
    let attributes = extractFromProgram(program,
        GL.getProgramParameter(program, GL.ACTIVE_ATTRIBUTES),
        GL.getActiveAttrib.bind(GL),
        GL.getAttribLocation.bind(GL)
    );

    let attribute_data = {};

    // 
    attributes.forEach((attribute, index) => {
        // Store Attribute Data
        attribute_data[attribute.info.name] = {
            location: attribute.location,
            info: Object.freeze({
                size: attribute.info.size,
                type: GLSLTypeMap[attribute.info.type],
                name: attribute.info.name
            })
        };
    });

    // Prevent modifications to the attribute information
    return Object.freeze(attribute_data);
}

function extractUniforms(program) {

    // Reads uniforms from GLSL
    let uniforms = extractFromProgram(program,
        GL.getProgramParameter(program, GL.ACTIVE_UNIFORMS),
        GL.getActiveUniform.bind(GL),
        GL.getUniformLocation.bind(GL)
    );

    let uniform_data = {};

    let unit = 0;
    for (let uniform of uniforms) {

        let info = {
            size: uniform.info.size,
            location: uniform.location,
            type: GLSLTypeMap[uniform.info.type],
            name: uniform.info.name
        };

        // 
        if (info.type.startsWith("sampler"))
            info.textureUnit = unit++;

        // Store Uniform Data
        uniform_data[info.name] = Object.freeze(info);
    }

    return Object.freeze(uniform_data);
}

function getUniformTree(uniforms) {

    let tree = {};

    // Construct tree from uniform data
    for (let [info, segments] of Object.values(uniforms).map(info => [info, extractSegments(info)])) {

        let uniform_ref = new Uniform(info);

        let map = tree;
        for (let i = 0; i < segments.length; i++) {
            let segment = segments[i];

            const name = segment.identifier;

            // Get the container for the id
            if (segment.access !== undefined && info.size == 1) {
                // Get/Create struct container
                if (name in map == false) map[name] = {};
                // Get/Create element container
                if (segment.access in map[name] == false) map[name][segment.access] = {};
                // Compute length
                map[name].length = Math.max(map[name].length || 1, segment.access + 1);
                // Descend into element container
                map = map[name][segment.access];
            } else {
                // Write endpoint ( single or primitive array )
                map[name] = uniform_ref;
            }
        }
    }

    // 
    return Arrayify(tree);
}

export default class Shader {

    /**
     * Compiles and links and new GLSL Shader Program.
     * @param {String} vert GLSL Vertex Shader Source
     * @param {String} frag GLSL Fragment Shader Source
     */
    constructor(vert, frag) {

        // Compile Shaders ( TODO: cache? )
        let vs = compileShader(vert, GL.VERTEX_SHADER);
        let fs = compileShader(frag, GL.FRAGMENT_SHADER);
        this.program = compileProgram(vs, fs);

        // Extract Shader Program Info  
        this.info = Object.freeze({
            shader: Object.freeze({ vertex: vs, fragment: fs }),
            attributes: extractAttributes(this.program),
            uniforms: extractUniforms(this.program)
        });

        // 
        this.uniforms = Object.freeze(getUniformTree(this.info.uniforms));
    }

    enable() {
        // Use the program
        // TODO: Check if already active and don't call to reduce overhead
        GL.useProgram(this.program);

        // Updates any changed uniform values
        for (let name in this.uniforms) {
            let uniform = this.uniforms[name];
            if (uniform.changed) {
                // console.log(`Shader setting uniform '${name}' to ${uniform.value}.`);
                uniform.update();
            }
        }
    }
}
