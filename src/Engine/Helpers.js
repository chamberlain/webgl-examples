export function Debounce(func, wait, immediate) {
    var timeout;
    return () => {
        var context = this, args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            timeout = null;
            if (!immediate) func.apply(context, args);
        }, wait);
        if (immediate && !timeout) func.apply(context, args);
    };
}

/**
 * Recursively convert array-like dicts to arrays.
 */
export function Arrayify(node) {
    // If frozen, simply return the frozen object.
    if (Object.isFrozen(node)) return node;
    else {

        // Arrayify children
        for (let name in node)
            node[name] = Arrayify(node[name]);

        // Convert array-like objects with a length to actual array.
        if (typeof node === "object" && node.length)
            node = Array.from(node);

        return node;
    }
}

// 
let _randomSeed = new Date().getTime();
let _currentSeed = _randomSeed;

/**
 * Set a new random seed.
 * @param {Number} seed New random base seed
 */
export function setRandomSeed(seed) {
    _currentSeed = _randomSeed = seed;
    
    _currentSeed = _currentSeed % 2147483647;
    if (_currentSeed <= 0)
        _currentSeed += 2147483646;
}

/**
 * Get the current random seed.
 */
export function getRandomSeed() {
    return _randomSeed;
}

/**
 * Get the next random number.
 */
export function random() {
    _currentSeed = _currentSeed * 16807 % 2147483647;
    return _currentSeed / 2147483646;
}

/**
 * Ensures the given value is an array. 
 * - If already an array, it is simply returned. 
 * - If it is not an array, returns the value wrapped by an array.
 * @param {*} item Some item or array of items
 */
export function EnsureArray(item) {
    if (Array.isArray(item)) return item;
    else return [item];
}

// Yup
export const getTypeOf = o => {
    if (Array.isArray(o)) return "array";
    else return typeof o;
}