

// Modified ES6 Port of https://github.com/evanw/lightgl.js/blob/master/src/vector.js
export default class Vector {
    /**
     * Creates a new 3d vector.
     * @param {Number} [x] X Coordinate
     * @param {Number} [y] Y Coordinate
     * @param {Number} [z] Z Coordinate
     */
    constructor(x, y, z) {
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
    }

    static get UnitX() { return UNIT_X; }

    static get UnitY() { return UNIT_Y; }

    static get UnitZ() { return UNIT_Z; }

    static get One() { return ONE; }

    static get Zero() { return ZERO; }

    set(x, y, z) {
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
    }

    add(v) {
        if (v instanceof Vector) return new Vector(this.x + v.x, this.y + v.y, this.z + v.z);
        else return new Vector(this.x + v, this.y + v, this.z + v);
    }

    subtract(v) {
        if (v instanceof Vector) return new Vector(this.x - v.x, this.y - v.y, this.z - v.z);
        else return new Vector(this.x - v, this.y - v, this.z - v);
    }

    multiply(v) {
        if (v instanceof Vector) return new Vector(this.x * v.x, this.y * v.y, this.z * v.z);
        else return new Vector(this.x * v, this.y * v, this.z * v);
    }

    divide(v) {
        if (v instanceof Vector) return new Vector(this.x / v.x, this.y / v.y, this.z / v.z);
        else return new Vector(this.x / v, this.y / v, this.z / v);
    }

    equals(v) {
        return this.x == v.x && this.y == v.y && this.z == v.z;
    }

    dot(v) {
        return this.x * v.x + this.y * v.y + this.z * v.z;
    }

    cross(v) {
        return new Vector(this.y * v.z - this.z * v.y, this.z * v.x - this.x * v.z, this.x * v.y - this.y * v.x);
    }

    negate() {
        return new Vector(-this.x, -this.y, -this.z);
    }

    get length() {
        return Math.sqrt(this.dot(this));
    }

    normalized(result = undefined) {
        result = result || new Vector();
        return Vector.normalize(this, result);
    }

    min() {
        return Math.min(Math.min(this.x, this.y), this.z);
    }

    max() {
        return Math.max(Math.max(this.x, this.y), this.z);
    }

    toAngles() {
        return {
            theta: Math.atan2(this.z, this.x),
            phi: Math.asin(this.y / this.length())
        };
    }

    angleTo(a) {
        return Math.acos(this.dot(a) / (this.length() * a.length()));
    }

    toArray(n) {
        return [this.x, this.y, this.z].slice(0, n || 3);
    }

    clone() {
        return new Vector(this.x, this.y, this.z);
    }

    set(x, y, z) {
        this.x = x;
        this.y = y;
        this.z = z;
        return this;
    }

    static fromAngles(theta, phi) {
        return new Vector(Math.cos(theta) * Math.cos(phi), Math.sin(phi), Math.sin(theta) * Math.cos(phi));
    }

    static fromArray(a) {
        return new Vector(a[0], a[1], a[2]);
    }

    static orthoNormalize(f, u) {
        f = f.normalized();
        let v = Vector.normalize(f.cross(u));
        return v.cross(f);
    }

    static randomDirection() {
        return Vector.fromAngles(Math.random() * Math.PI * 2, Math.asin(Math.random() * 2 - 1));
    }

    static min(a, b) {
        return new Vector(Math.min(a.x, b.x), Math.min(a.y, b.y), Math.min(a.z, b.z));
    }

    static max(a, b) {
        return new Vector(Math.max(a.x, b.x), Math.max(a.y, b.y), Math.max(a.z, b.z));
    }

    static distance(a, b) {
        let dx = a.x - b.x;
        let dy = a.y - b.y;
        let dz = a.z - b.z;
        return Math.sqrt((dx * dx) + (dy * dy) + (dz * dz));
    }

    static normalize(v, result = undefined) {
        result = result || new Vector();

        const m = 1.0 / v.length;

        // Store normalized vector
        result.x = v.x * m;
        result.y = v.y * m;
        result.z = v.z * m;

        return result;
    }
}

const ZERO = Object.freeze(new Vector(0, 0, 0))
const UNIT_X = Object.freeze(new Vector(1, 0, 0));
const UNIT_Y = Object.freeze(new Vector(0, 1, 0));
const UNIT_Z = Object.freeze(new Vector(0, 0, 1));
const ONE = Object.freeze(new Vector(1, 1, 1));;