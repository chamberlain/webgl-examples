// Modified ES6 Port of https://github.com/evanw/lightgl.js/blob/master/src/vector.js
import Vector from './Vector.js';

export default class Quaternion {
    /**
     * Creates a new quaternion.
     * @param {Number} [x] X Component
     * @param {Number} [y] Y Component
     * @param {Number} [z] Z Component
     * @param {Number} [w] W Component
     */
    constructor(x, y, z, w) {
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
        this.w = w || 0;
    }

    multiply(v) {
        return Quaternion.multiply(this, v);
    }

    equals(v) {
        return this.x == v.x
            && this.y == v.y
            && this.z == v.z
            && this.w == v.w;
    }

    normalized() {
        return Quaternion.normalize(this);
    }

    get length() {
        let sqrLength = (this.x * this.x) + (this.y * this.y) + (this.z * this.z) + (this.w * this.w);
        return Math.sqrt(sqrLength);
    }

    clone() {
        return new Quaternion(this.x, this.y, this.z, this.w);
    }

    static identity(result) {
        result = result || new Quaternion();
        result.x = result.y = result.z = 0;
        result.w = 1;
        return result;
    }

    static multiply(a, b, result) {
        result = result || new Quaternion();

        let ax = a.x, ay = a.y, az = a.z, aw = a.w;
        let bx = b.x, by = b.y, bz = b.z, bw = b.w;

        result.x = ax * bw + aw * bx + ay * bz - az * by;
        result.y = ay * bw + aw * by + az * bx - ax * bz;
        result.z = az * bw + aw * bz + ax * by - ay * bx;
        result.w = aw * bw - ax * bx - ay * by - az * bz;

        return result;
    }

    static toAxisAngle(q, result) {
        result = result || new Vector();

        let rad = Math.acos(q.w) * 2.0;
        let s = Math.sin(rad / 2.0);

        if (s != 0.0) {
            result.x = q.x / s;
            result.y = q.y / s;
            result.z = q.z / s;
        } else {
            // If s is zero, return any axis (no rotation - axis does not matter)
            result.x = 1;
            result.y = 0;
            result.z = 0;
        }

        return { axis: result, angle: rad };
    }

    static fromAxisAngle(axis, angle, result) {
        result = result || new Quaternion();

        angle = angle * 0.5;

        let s = Math.sin(angle);
        result.x = s * axis.x;
        result.y = s * axis.y;
        result.z = s * axis.z;
        result.w = Math.cos(angle);

        return result;
    }

    static rotateTo(from, dest) {
        let m = Math.sqrt(2.0 + 2.0 * from.dot(dest));
        let w = from.cross(dest).multiply(1.0 / m);
        return new Quaternion(w.x, w.y, w.z, 0.5 * m);
    }

    static lookRotation(f, u, result) {
        result = result || new Quaternion();
        u = u || Vector.UnitZ;

        // 
        u = Vector.orthoNormalize(f, u);
        let r = u.cross(f);

        result.w = Math.sqrt(1.0 + r.x + u.y + f.z) * 0.5;

        let w4_recip = 1.0 / (4.0 * result.w);
        result.x = (u.z - f.y) * w4_recip;
        result.y = (f.x - r.z) * w4_recip;
        result.z = (r.y - u.x) * w4_recip;

        return result;
    }


    static normalize(q) {

        let m = 1.0 / q.length;

        let x = q.x * m;
        let y = q.y * m;
        let z = q.z * m;
        let w = q.w * m;

        return new Quaternion(x, y, z, w);
    }

    static slerp(a, b, t, result) {
        result = result || new Quaternion();

        let ax = a.x, ay = a.y, az = a.z, aw = a.w;
        let bx = b.x, by = b.y, bz = b.z, bw = b.w;

        let omega, cosom, sinom, scale0, scale1;

        // calc cosine
        cosom = ax * bx + ay * by + az * bz + aw * bw;

        // adjust signs (if necessary)
        if (cosom < 0.0) {
            cosom = -cosom;
            bx = - bx;
            by = - by;
            bz = - bz;
            bw = - bw;
        }

        // calculate coefficients
        if ((1.0 - cosom) > 0.000001) {
            // standard case (slerp)
            omega = Math.acos(cosom);
            sinom = Math.sin(omega);
            scale0 = Math.sin((1.0 - t) * omega) / sinom;
            scale1 = Math.sin(t * omega) / sinom;
        } else {
            // "from" and "to" quaternions are very close
            //  ... so we can do a linear interpolation
            scale0 = 1.0 - t;
            scale1 = t;
        }
        // calculate final values
        result.x = scale0 * ax + scale1 * bx;
        result.y = scale0 * ay + scale1 * by;
        result.z = scale0 * az + scale1 * bz;
        result.w = scale0 * aw + scale1 * bw;

        return result;
    }

    static invert(a, result) {
        result = result || new Quaternion();

        let a0 = a.x, a1 = a.y, a2 = a.z, a3 = a.w;
        let dot = a0 * a0 + a1 * a1 + a2 * a2 + a3 * a3;

        if (dot == 0.0) {
            result.x = 0;
            result.y = 0;
            result.z = 0;
            result.w = 0;
        } else {
            let invDot = dot ? 1.0 / dot : 0;

            result.x = -a0 * invDot;
            result.x = -a1 * invDot;
            result.z = -a2 * invDot;
            result.w = a3 * invDot;
        }

        return result;
    }

    static conjugate(a, result) {
        result = result || new Quaternion();

        result.x = -a.x;
        result.y = -a.y;
        result.z = -a.z;
        result.w = a.w;

        return result;
    }
}

const IDENTITY = Object.freeze(new Quaternion(0, 0, 0, 1))