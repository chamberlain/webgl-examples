import Vector from "./Vector.js";
import Quaternion from "./Quaternion.js";

// ES6 Port of https://github.com/evanw/lightgl.js/blob/master/src/matrix.js

var hasFloat32Array = typeof Float32Array != "undefined";

export default class Matrix {

    /**
     * Creates a new 4x4 matrix.
     */
    constructor() {
        var m = Array.prototype.concat.apply([], arguments);
        if (!m.length) {
            m = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1];
        }

        this.m = hasFloat32Array ? new Float32Array(m) : m;
    }

    // Returns the matrix that when multiplied with this matrix results in the
    // identity matrix.
    inverse() {
        return Matrix.inverse(this, new Matrix());
    }

    // Returns this matrix, exchanging columns for rows.
    transpose() {
        return Matrix.transpose(this, new Matrix());
    }

    // Returns the concatenation of the transforms for this matrix and `matrix`.
    // This emulates the OpenGL function `glMultMatrix()`.
    multiply(matrix) {
        return Matrix.multiply(this, matrix, new Matrix());
    }

    // Transforms the vector as a point with a w coordinate of 1. This
    // means translations will have an effect, for example.
    transformPoint(v) {
        var m = this.m;
        return new Vector(
            m[0] * v.x + m[1] * v.y + m[2] * v.z + m[3],
            m[4] * v.x + m[5] * v.y + m[6] * v.z + m[7],
            m[8] * v.x + m[9] * v.y + m[10] * v.z + m[11]
        ).divide(m[12] * v.x + m[13] * v.y + m[14] * v.z + m[15]);
    }

    // Transforms the vector as a vector with a w coordinate of 0. This
    // means translations will have no effect, for example.
    transformVector(v) {
        var m = this.m;
        return new Vector(
            m[0] * v.x + m[1] * v.y + m[2] * v.z,
            m[4] * v.x + m[5] * v.y + m[6] * v.z,
            m[8] * v.x + m[9] * v.y + m[10] * v.z
        );
    }

    // Returns the matrix that when multiplied with `matrix` results in the
    // identity matrix. You can optionally pass an existing matrix in `result`
    // to avoid allocating a new matrix. This implementation is from the Mesa
    // OpenGL function `__gluInvertMatrixd()` found in `project.c`.
    static inverse(matrix, result) {
        result = result || new Matrix();

        // let a = matrix.m;

        // let a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3];
        // let a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7];
        // let a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11];
        // let a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15];

        // let b00 = a00 * a11 - a01 * a10;
        // let b01 = a00 * a12 - a02 * a10;
        // let b02 = a00 * a13 - a03 * a10;
        // let b03 = a01 * a12 - a02 * a11;
        // let b04 = a01 * a13 - a03 * a11;
        // let b05 = a02 * a13 - a03 * a12;
        // let b06 = a20 * a31 - a21 * a30;
        // let b07 = a20 * a32 - a22 * a30;
        // let b08 = a20 * a33 - a23 * a30;
        // let b09 = a21 * a32 - a22 * a31;
        // let b10 = a21 * a33 - a23 * a31;
        // let b11 = a22 * a33 - a23 * a32;

        // // Calculate the determinant
        // let det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

        // if (!det) {
        //     return null;
        // }
        // det = 1.0 / det;

        // let out = result.m;
        // out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
        // out[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
        // out[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
        // out[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
        // out[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
        // out[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
        // out[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
        // out[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
        // out[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
        // out[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
        // out[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
        // out[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
        // out[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
        // out[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
        // out[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
        // out[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;

        // return result;

        var m = matrix.m, r = result.m;

        r[0] = m[5] * m[10] * m[15] - m[5] * m[14] * m[11] - m[6] * m[9] * m[15] + m[6] * m[13] * m[11] + m[7] * m[9] * m[14] - m[7] * m[13] * m[10];
        r[1] = -m[1] * m[10] * m[15] + m[1] * m[14] * m[11] + m[2] * m[9] * m[15] - m[2] * m[13] * m[11] - m[3] * m[9] * m[14] + m[3] * m[13] * m[10];
        r[2] = m[1] * m[6] * m[15] - m[1] * m[14] * m[7] - m[2] * m[5] * m[15] + m[2] * m[13] * m[7] + m[3] * m[5] * m[14] - m[3] * m[13] * m[6];
        r[3] = -m[1] * m[6] * m[11] + m[1] * m[10] * m[7] + m[2] * m[5] * m[11] - m[2] * m[9] * m[7] - m[3] * m[5] * m[10] + m[3] * m[9] * m[6];

        r[4] = -m[4] * m[10] * m[15] + m[4] * m[14] * m[11] + m[6] * m[8] * m[15] - m[6] * m[12] * m[11] - m[7] * m[8] * m[14] + m[7] * m[12] * m[10];
        r[5] = m[0] * m[10] * m[15] - m[0] * m[14] * m[11] - m[2] * m[8] * m[15] + m[2] * m[12] * m[11] + m[3] * m[8] * m[14] - m[3] * m[12] * m[10];
        r[6] = -m[0] * m[6] * m[15] + m[0] * m[14] * m[7] + m[2] * m[4] * m[15] - m[2] * m[12] * m[7] - m[3] * m[4] * m[14] + m[3] * m[12] * m[6];
        r[7] = m[0] * m[6] * m[11] - m[0] * m[10] * m[7] - m[2] * m[4] * m[11] + m[2] * m[8] * m[7] + m[3] * m[4] * m[10] - m[3] * m[8] * m[6];

        r[8] = m[4] * m[9] * m[15] - m[4] * m[13] * m[11] - m[5] * m[8] * m[15] + m[5] * m[12] * m[11] + m[7] * m[8] * m[13] - m[7] * m[12] * m[9];
        r[9] = -m[0] * m[9] * m[15] + m[0] * m[13] * m[11] + m[1] * m[8] * m[15] - m[1] * m[12] * m[11] - m[3] * m[8] * m[13] + m[3] * m[12] * m[9];
        r[10] = m[0] * m[5] * m[15] - m[0] * m[13] * m[7] - m[1] * m[4] * m[15] + m[1] * m[12] * m[7] + m[3] * m[4] * m[13] - m[3] * m[12] * m[5];
        r[11] = -m[0] * m[5] * m[11] + m[0] * m[9] * m[7] + m[1] * m[4] * m[11] - m[1] * m[8] * m[7] - m[3] * m[4] * m[9] + m[3] * m[8] * m[5];

        r[12] = -m[4] * m[9] * m[14] + m[4] * m[13] * m[10] + m[5] * m[8] * m[14] - m[5] * m[12] * m[10] - m[6] * m[8] * m[13] + m[6] * m[12] * m[9];
        r[13] = m[0] * m[9] * m[14] - m[0] * m[13] * m[10] - m[1] * m[8] * m[14] + m[1] * m[12] * m[10] + m[2] * m[8] * m[13] - m[2] * m[12] * m[9];
        r[14] = -m[0] * m[5] * m[14] + m[0] * m[13] * m[6] + m[1] * m[4] * m[14] - m[1] * m[12] * m[6] - m[2] * m[4] * m[13] + m[2] * m[12] * m[5];
        r[15] = m[0] * m[5] * m[10] - m[0] * m[9] * m[6] - m[1] * m[4] * m[10] + m[1] * m[8] * m[6] + m[2] * m[4] * m[9] - m[2] * m[8] * m[5];

        var det = m[0] * r[0] + m[1] * r[4] + m[2] * r[8] + m[3] * r[12];
        for (var i = 0; i < 16; i++) r[i] /= det;
        return result;
    }

    // Returns `matrix`, exchanging columns for rows. You can optionally pass an
    // existing matrix in `result` to avoid allocating a new matrix.
    static transpose(matrix, result) {
        result = result || new Matrix();
        var m = matrix.m, r = result.m;
        r[0] = m[0]; r[1] = m[4]; r[2] = m[8]; r[3] = m[12];
        r[4] = m[1]; r[5] = m[5]; r[6] = m[9]; r[7] = m[13];
        r[8] = m[2]; r[9] = m[6]; r[10] = m[10]; r[11] = m[14];
        r[12] = m[3]; r[13] = m[7]; r[14] = m[11]; r[15] = m[15];
        return result;
    }

    // Returns the concatenation of the transforms for `left` and `right`. You can
    // optionally pass an existing matrix in `result` to avoid allocating a new
    // matrix.
    static multiply(left, right, result) {
        result = result || new Matrix();

        var a = left.m, b = right.m, r = result.m;

        r[0] = a[0] * b[0] + a[1] * b[4] + a[2] * b[8] + a[3] * b[12];
        r[1] = a[0] * b[1] + a[1] * b[5] + a[2] * b[9] + a[3] * b[13];
        r[2] = a[0] * b[2] + a[1] * b[6] + a[2] * b[10] + a[3] * b[14];
        r[3] = a[0] * b[3] + a[1] * b[7] + a[2] * b[11] + a[3] * b[15];

        r[4] = a[4] * b[0] + a[5] * b[4] + a[6] * b[8] + a[7] * b[12];
        r[5] = a[4] * b[1] + a[5] * b[5] + a[6] * b[9] + a[7] * b[13];
        r[6] = a[4] * b[2] + a[5] * b[6] + a[6] * b[10] + a[7] * b[14];
        r[7] = a[4] * b[3] + a[5] * b[7] + a[6] * b[11] + a[7] * b[15];

        r[8] = a[8] * b[0] + a[9] * b[4] + a[10] * b[8] + a[11] * b[12];
        r[9] = a[8] * b[1] + a[9] * b[5] + a[10] * b[9] + a[11] * b[13];
        r[10] = a[8] * b[2] + a[9] * b[6] + a[10] * b[10] + a[11] * b[14];
        r[11] = a[8] * b[3] + a[9] * b[7] + a[10] * b[11] + a[11] * b[15];

        r[12] = a[12] * b[0] + a[13] * b[4] + a[14] * b[8] + a[15] * b[12];
        r[13] = a[12] * b[1] + a[13] * b[5] + a[14] * b[9] + a[15] * b[13];
        r[14] = a[12] * b[2] + a[13] * b[6] + a[14] * b[10] + a[15] * b[14];
        r[15] = a[12] * b[3] + a[13] * b[7] + a[14] * b[11] + a[15] * b[15];

        return result;
    }

    // Returns an identity matrix. You can optionally pass an existing matrix in
    // `result` to avoid allocating a new matrix.
    static identity(result) {
        result = result || new Matrix();
        var m = result.m;
        m[0] = m[5] = m[10] = m[15] = 1;
        m[1] = m[2] = m[3] = m[4] = m[6] = m[7] = m[8] = m[9] = m[11] = m[12] = m[13] = m[14] = 0;
        return result;
    }

    // Returns a perspective transform matrix, which makes far away objects appear
    // smaller than nearby objects. The `aspect` argument should be the width
    // divided by the height of your viewport and `fov` is the top-to-bottom angle
    // of the field of view in radians. You can optionally pass an existing matrix
    // in `result` to avoid allocating a new matrix.
    static perspective(fovy, aspect, near, far, result) {

        result = result || new Matrix();
        var out = result.m;

        let f = 1.0 / Math.tan(fovy / 2);
        let nf = 1 / (near - far);
        out[0] = f / aspect;
        // out[1] = 0;
        // out[2] = 0;
        // out[3] = 0;
        // out[4] = 0;
        out[5] = f;
        // out[6] = 0;
        // out[7] = 0;
        // out[8] = 0;
        // out[9] = 0;
        out[10] = (far + near) * nf;
        out[11] = -1;
        // out[12] = 0;
        // out[13] = 0;
        out[14] = (2 * far * near) * nf;
        // out[15] = 0;

        return result;
    }

    // Returns an orthographic projection, in which objects are the same size no
    // matter how far away or nearby they are. You can optionally pass an existing
    // matrix in `result` to avoid allocating a new matrix. This emulates the OpenGL
    // function `glOrtho()`.
    static ortho(l, r, b, t, n, f, result) {
        result = result || new Matrix();
        var m = result.m;

        m[0] = 2 / (r - l);
        m[1] = 0;
        m[2] = 0;
        m[3] = -(r + l) / (r - l);

        m[4] = 0;
        m[5] = 2 / (t - b);
        m[6] = 0;
        m[7] = -(t + b) / (t - b);

        m[8] = 0;
        m[9] = 0;
        m[10] = -2 / (f - n);
        m[11] = -(f + n) / (f - n);

        m[12] = 0;
        m[13] = 0;
        m[14] = 0;
        m[15] = 1;

        return result;
    }

    // Returns a matrix that puts the camera at the eye point looking
    // toward the center point.
    // You can optionally pass an existing matrix in `result` to avoid allocating
    // a new matrix.
    static lookAt(eye, center, up, result) {
        result = result || new Matrix();
        up = up || Vector.UnitZ;

        let z = eye.subtract(center).normalized();
        let x = up.cross(z).normalized();
        let y = z.cross(x).normalized();

        let m = result.m;

        m[0] = x.x;
        m[1] = y.x;
        m[2] = z.x;
        m[3] = 0;

        m[4] = x.y;
        m[5] = y.y;
        m[6] = z.y;
        m[7] = 0;

        m[8] = x.z;
        m[9] = y.z;
        m[10] = z.z;
        m[11] = 0;

        m[12] = -x.dot(eye);
        m[13] = -y.dot(eye);
        m[14] = -z.dot(eye);
        m[15] = 1;

        return result;
    }

    // This emulates the OpenGL function `glScale()`. You can optionally pass an
    // existing matrix in `result` to avoid allocating a new matrix.
    static scale(x, y, z, result) {
        result = result || new Matrix();

        var m = result.m;

        m[0] = x;
        m[1] = 0;
        m[2] = 0;
        m[3] = 0;

        m[4] = 0;
        m[5] = y;
        m[6] = 0;
        m[7] = 0;

        m[8] = 0;
        m[9] = 0;
        m[10] = z;
        m[11] = 0;

        m[12] = 0;
        m[13] = 0;
        m[14] = 0;
        m[15] = 1;

        return result;
    }

    // This emulates the OpenGL function `glTranslate()`. You can optionally pass
    // an existing matrix in `result` to avoid allocating a new matrix.
    static translate(x, y, z, result) {
        result = result || new Matrix();
        var m = result.m;

        m[0] = 1;
        m[1] = 0;
        m[2] = 0;
        m[3] = 0;

        m[4] = 0;
        m[5] = 1;
        m[6] = 0;
        m[7] = 0;

        m[8] = 0;
        m[9] = 0;
        m[10] = 1;
        m[11] = 0;

        m[12] = x;
        m[13] = y;
        m[14] = z;
        m[15] = 1;

        return result;
    }

    // Returns a matrix that rotates by `angle` radians around the vector `x, y, z`.
    // You can optionally pass an existing matrix in `result` to avoid allocating
    // a new matrix. This emulates the OpenGL function `glRotate()`.
    static rotate(angle, x, y, z, result) {
        if (!angle || (!x && !y && !z)) {
            return Matrix.identity(result);
        }

        result = result || new Matrix();
        var m = result.m;

        var d = Math.sqrt(x * x + y * y + z * z);
        x /= d; y /= d; z /= d;
        var c = Math.cos(angle), s = Math.sin(angle), t = 1 - c;

        m[0] = x * x * t + c;
        m[1] = x * y * t - z * s;
        m[2] = x * z * t + y * s;
        m[3] = 0;

        m[4] = y * x * t + z * s;
        m[5] = y * y * t + c;
        m[6] = y * z * t - x * s;
        m[7] = 0;

        m[8] = z * x * t - y * s;
        m[9] = z * y * t + x * s;
        m[10] = z * z * t + c;
        m[11] = 0;

        m[12] = 0;
        m[13] = 0;
        m[14] = 0;
        m[15] = 1;

        return result;
    }

    static fromQuaternion(q, result) {
        result = result || Matrix.identity();

        let r = Quaternion.toAxisAngle(q);
        return Matrix.rotate(r.angle, r.axis.x, r.axis.y, r.axis.z);
    }
} 