import { Debounce } from "./Helpers.js";

let ContextMap = {};

/**
 * Gets the most modern WebGL context available.
 * @param {HTMLCanvasElement} canvas Some canvas
 */
function getGraphicsContext(canvas) {
    // Try to get existing context
    if (canvas in ContextMap) return ContextMap[canvas];
    // Unable to find existing context, get it from the canvas.
    else {
        // Try to get a WebGL 2 Context
        // let context = canvas.getContext("webgl2");
        // if (context) {
        //     ContextMap[canvas] = context;
        //     return context;
        // }

        // Try to get a WebGL 1 Context
        let context = canvas.getContext("webgl");
        if (context) {
            ContextMap[canvas] = context;
            return context;
        }

        // No WebGL, Damn!
        throw new Error("Unable to acquire WebGL 2 context.");
    }
}

function getCanvasElement(selector) {
    if (selector instanceof HTMLCanvasElement) return selector;
    else return document.querySelector(selector);
}

//
let GL = undefined;
let Canvas = undefined;

let Context = {
    /** 
     * The current WebGL context 
     */
    get GL() {
        return GL;
    },
    // Get the canvas
    get Canvas() {
        return Canvas;
    },
    //
    SetCanvas(selector, autoResize = true) {
        // Check if the context was already initialized
        if (GL) throw new Error(`GL already initialized on ${Canvas}.`);

        // Get canvas element
        Canvas = getCanvasElement(selector);

        // Get context
        GL = getGraphicsContext(Canvas);

        // Backface Culling
        // GL.enable(GL.CULL_FACE);

        // Z-Buffer ( Depth Testing )
        GL.enable(GL.DEPTH_TEST);
        GL.depthFunc(GL.LEQUAL);
    },

    SetFullscreen(selector) {
        // Find canvas element
        let canvas = getCanvasElement(selector);

        window.addEventListener("resize", Debounce(() => {
            canvas.width = window.innerWidth / (1*window.devicePixelRatio || 1);
            canvas.height = window.innerHeight / (1*window.devicePixelRatio || 1);
        }, 50));

        // Trigger resize for initial adjustment
        window.dispatchEvent(new Event("resize"));
    },

    SetRenderCallback(renderCallback) {
        window.requestAnimationFrame(function renderLoop() {
            // Request to render again next frame
            window.requestAnimationFrame(renderLoop);
            renderCallback();
        });
    }
};

export { GL, Canvas, Context };
export default Context;
