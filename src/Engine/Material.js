import { GL } from "./Context.js";
import Texture from "./Texture.js";

const _private = new WeakMap();

export default class Material {
    // TODO: Blend settings ( RenderQueue/Sorting )
    // TODO: Uniform override
    constructor(shader) {

        let uniform_data = {};

        _private.set(this, {
            shader,
            uniform_data
        });

        //
        for (var name in _private.get(this).shader.uniforms) {

            uniform_data[name] = {
                value: undefined,
                changed: false
            };

            // Define getter and setters for the uniforms
            Object.defineProperty(this, name, {
                //
                enumerable: true,
                configurable: false,
                //
                get: () => uniform_data[name].value,
                set: value => {
                    let uniform = uniform_data[name]
                    uniform.changed = true;
                    uniform.value = value;
                }
            });
        }
    }

    enable() {

        let shader = _private.get(this).shader;
        let uniform_data = _private.get(this).uniform_data;

        let unit = 0;
        for (var name in uniform_data) {

            // 
            let uniform = uniform_data[name];

            // 
            if (uniform.changed) {

                // TODO: Apply textures in shader class?
                if (uniform.value instanceof Texture) {
                    shader.uniforms[name] = unit;
                    uniform.value.use(unit++);
                } else {
                    shader.uniforms[name] = uniform.value;
                }
                uniform.changed = false;
            }
        }

        shader.enable();
    }
}
