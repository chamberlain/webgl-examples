import { GL } from "./Context.js";

export default class Uniform {
    
        constructor(info) {
            this.info = info;
            this.value = undefined;
            this.changed = false;
        }
    
        get() {
            return this.value;
        }
    
        set(val) {
            this.changed = true;
            this.value = val;
        }
    
        update() {
            switch (this.info.type) {
    
                // Float
                case "float":
                    if (this.info.size > 1) GL.uniform1fv(this.info.location, this.value);
                    else GL.uniform1f(this.info.location, this.value);
                    break;
    
                case "vec2":
                    if ('x' in this.value) GL.uniform2f(this.info.location, this.value.x, this.value.y);
                    else GL.uniform2fv(this.info.location, this.value);
                    break;
    
                case "vec3":
                    if ('x' in this.value) GL.uniform3f(this.info.location, this.value.x, this.value.y, this.value.z);
                    else GL.uniform3fv(this.info.location, this.value);
                    break;
    
                case "vec4":
                    if ('x' in this.value) GL.uniform4f(this.info.location, this.value.x, this.value.y, this.value.z, this.value.w);
                    else GL.uniform4fv(this.info.location, this.value);
                    break;
    
                // Integer 
                case "int":
                    if (this.info.size > 1) GL.uniform1iv(this.info.location, this.value);
                    else GL.uniform1i(this.info.location, this.value);
                    break;
    
                case "ivec2":
                    if ('x' in this.value) GL.uniform2i(this.info.location, this.value.x, this.value.y);
                    else GL.uniform2iv(this.info.location, this.value);
                    break;
    
                case "ivec3":
                    if ('x' in this.value) GL.uniform3i(this.info.location, this.value.x, this.value.y, this.value.z);
                    else GL.uniform3iv(this.info.location, this.value);
                    break;
    
                case "ivec4":
                    if ('x' in this.value) GL.uniform4i(this.info.location, this.value.x, this.value.y, this.value.z, this.value.w);
                    else GL.uniform4iv(this.info.location, this.value);
                    break;
    
                // Boolean
                case "bool":
                    if (this.info.size > 1) GL.uniform1iv(this.info.location, this.value);
                    else GL.uniform1i(this.info.location, this.value);
                    break;
    
                case "bvec2":
                    if ('x' in this.value) GL.uniform2i(this.info.location, this.value.x, this.value.y);
                    else GL.uniform2iv(this.info.location, this.value);
                    break;
    
                case "bvec3":
                    if ('x' in this.value) GL.uniform3i(this.info.location, this.value.x, this.value.y, this.value.z);
                    else GL.uniform3iv(this.info.location, this.value);
                    break;
    
                case "bvec4":
                    if ('x' in this.value) GL.uniform4i(this.info.location, this.value.x, this.value.y, this.value.z, this.value.w);
                    else GL.uniform4iv(this.info.location, this.value);
                    break;
    
                // Matrix
                case "mat2":
                    if ('m' in this.value) GL.uniformMatrix2fv(this.info.location, false, this.value.m);
                    else GL.uniformMatrix2fv(this.info.location, false, this.value);
                    break;
    
                case "mat3":
                    if ('m' in this.value) GL.uniformMatrix3fv(this.info.location, false, this.value.m);
                    else GL.uniformMatrix3fv(this.info.location, false, this.value);
                    break;
    
                case "mat4":
                    if ('m' in this.value) GL.uniformMatrix4fv(this.info.location, false, this.value.m);
                    else GL.uniformMatrix4fv(this.info.location, false, this.value);
                    break;
    
                // Texture
                case "sampler2D":
                    // May not be the best way, but DOES work everytime.
                    // Change to active texture
                    GL.uniform1i(this.info.location, this.info.textureUnit);
                    // Bind texture in correct unit
                    GL.activeTexture(GL.TEXTURE0 + this.info.textureUnit);
                    GL.bindTexture(GL.TEXTURE_2D, this.value.texture);
                    break;
    
                case "samplerCube":
                    // May not be the best way, but DOES work everytime.
                    // Change to active texture
                    GL.uniform1i(this.info.location, this.info.textureUnit);
                    // Bind texture in correct unit
                    GL.activeTexture(GL.TEXTURE0 + this.info.textureUnit);
                    GL.bindTexture(GL.TEXTURE_CUBE_MAP, this.value.texture);
                    break;
            }
    
            // Mark as updated
            this.changed = false;
        }
    }