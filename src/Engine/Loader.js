import TextureLoader from './Loaders/TextureLoader.js';
import ObjLoader from "./Loaders/ObjLoader.js";
import Texture from "./Texture.js";
import { getTypeOf, EnsureArray } from "./Helpers.js";

let CustomHandlers = {
    // Wavefront OBJ Moels
    "obj": ObjLoader,
    // Textures via HTML Images
    "jpg": TextureLoader,
    "png": TextureLoader,
    "bmp": TextureLoader,
    "gif": TextureLoader,
};

// Append loading indicator element
let progressElement = document.createElement("div");
progressElement.classList = "progress";
document.body.appendChild(progressElement);

/**
 * Displays a loading indicator.
 * @param {String} text The text to display
 */
export function ShowProgress(text) {
    progressElement.style.display = "block";
    progressElement.textContent = text || "";
}

/**
 * Hides the loading indicator.
 */
export function HideProgress() {
    progressElement.style.display = "none";
}

/**
 * Registers a custom loading handler for a specific file extension.
 * @param {String} ext File extension ( ex. 'png' )
 * @param {Object} handler An object with a load member
 */
export function RegisterCustomLoadHandler(ext, handler) {
    CustomHandlers[ext] = handler;
}

/**
 * Asynchronously loads a remote asset.
 * @param {String} url A standard HTTP GET request.
 * @param {String} type Response type.
 * @param {Function} progress Optional progress reporting function
 */
export function Get(url, type = "text", progress = undefined) {
    return new Promise((resolve, reject) => {

        var request = new XMLHttpRequest();
        request.responseType = type || "text";

        if (progress) {
            request.onprogress = ev => {
                if (ev.lengthComputable) progress(ev.loaded / ev.total);
                else progress(0.0); // No length...?
            };
        }

        request.onerror = () => reject({ status: response.status, message: response.statusText });
        request.onload = () => resolve(request.response);

        request.open('GET', url, true);
        request.send(null);
    });
}

/**
 * Loads an asset, and possibly processes it with a custom loading script.
 * 
 * @param {String} url Some URL to a remote asset.
 * @param {String} type Some type to use ( null for default )
 * @param {Function} progress Optional function for reporting load progress.
 */
function LoadAsset(url, type, progress) {
    let ext = url.split('.').pop();
    if (ext in CustomHandlers) return CustomHandlers[ext].Load(url, progress);
    else return Get(url, type, progress);
}

/**
 * Loads an hierachical set of assets.
 * @param {String|Object} manifest The path to a JSON file or an object describing asset heirarchy to load.
 */
function LoadManifest(manifest) {

    const loadWithManifest = manifest => {

        ShowProgress("Loading");

        // 
        return new Promise((resolve, reject) => {

            // 
            let total = 0, current = 0;
            let promises = [];

            // Load a single asset
            const load = (data, key, url) => {

                // When the asset is loaded
                let n = promises.push(LoadAsset(url, false, () => {
                    let progress = Math.floor((current / total) * 100);
                    ShowProgress(progress + "%");
                }));

                total++; // 
                promises[n - 1].then(value => {
                    data[key] = value;
                    current++;
                });
            }

            // Recursive loading
            const rec = (data) => {

                // For each entry in object
                for (let key in data) {
                    let item = data[key];
                    let type = getTypeOf(item);

                    // Object ( Containers )
                    if (type === "object") rec(item);
                    // Items ( Assets )
                    else if (type === "string") {
                        load(data, key, item);
                    } else if (type === "array") {

                        for (var i = 0; i < item.length; i++)
                            load(item, i, item[i]);

                    }
                }
            };

            // 
            rec(manifest);

            // When everything is loaded
            Promise.all(promises).then(() => {
                resolve(manifest);
                HideProgress();
            });
        });
    };

    // 
    return new Promise((resolve, reject) => {

        // Loading from JSON from url
        if (typeof manifest === "string") {

            // Loads the JSON
            Get(manifest, "text")
                .catch(error => reject)
                .then(text => {
                    // Load the manifest then resolve promise
                    loadWithManifest(JSON.parse(text))
                        .catch(error => reject)
                        .then(resolve);
                });
        }
        // Loading from manifest directly
        else if (typeof manifest === "object" && !Array.isArray(manifest)) {
            // Load, then resolve promise
            loadWithManifest(manifest)
                .catch(error => reject)
                .then(resolve);
        } else {
            // Don't know what to do with this.
            reject(`Unable to load assets from '${typeof manifest}'.`);
        }
    });
}

export default {
    Get, LoadManifest,
    RegisterCustomLoadHandler,
    ShowProgress, HideProgress
}