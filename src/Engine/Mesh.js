import { GL } from "./Context.js";
import { GLSLTypeMap, GLSLTypeBase, GLSLTypeSize } from "./Shader.js";

import VertexBuffer from "./VertexBuffer.js";
import IndexBuffer from "./IndexBuffer.js";
import Vector from "./Math/Vector.js";

/**
 * Default attribute layout for rendering meshes.
 */
export const DefaultMeshLayout = Object.freeze([
    { type: "vec3", name: "positions" },
    { type: "vec3", name: "normals" },
    { type: "vec2", name: "uvs" }
]);

export class Mesh {

    constructor(layout = DefaultMeshLayout) {
        this.layout = layout;

        let vertex_data = {};
        let element_data = {};

        let buffer_data = {
            element: {},
            vertex: {}
        };

        // Create vertex buffers
        for (var idx in this.layout) {
            var attrib = this.layout[idx].name;
            vertex_data[attrib] = [];
            buffer_data.vertex[attrib] = {
                index: +idx,
                buffer: new VertexBuffer(),
                changed: false
            };
        }

        // Create element buffers ( all primitives? )
        for (var mode of ["triangles", "lines"]) {
            element_data[mode] = [];
            buffer_data.element[mode] = {
                buffer: new IndexBuffer(),
                changed: false
            };
        }

        // Vertex Data Proxy
        this.vertices = new Proxy(vertex_data, {
            get(target, name) {
                return vertex_data[name];
            },
            set(target, name, value) {
                if (name in vertex_data) {
                    // Make falsy values an empty array
                    // TODO: Make any non-array an empty array?
                    if (!value) value = [];
                    // Mark buffer as changed, and set new data
                    buffer_data.vertex[name].changed = true;
                    vertex_data[name] = value;
                } else {
                    throw new Error(`Unable to assign vertex data to unknown attribute '${name}' in mesh.`);
                }
                return true;
            }
        });

        // Element Data Proxy
        this.elements = new Proxy(element_data, {
            get(target, name) {
                return target[name];
            },
            set(target, name, value) {

                if (name in element_data) {
                    // Make falsy values an empty array
                    // TODO: Make any non-array an empty array?
                    if (!value) value = [];
                    // Mark buffer as changed, and set new data
                    buffer_data.element[name].changed = true;
                    element_data[name] = value;
                } else {
                    throw new Error(`Unable to assign vertex data to unknown attribute '${name}' in mesh.`);
                }
                return true;
            }
        });

        this.buffers = buffer_data;
    }

    /**
     * Computes the lines for drawing a wireframe from the triangle elements.
     */
    computeWireframe() {

        let lines = [];

        const triangles = this.elements.triangles;
        for (let i = 0; i < triangles.length; i++) {
            lines.push(triangles[i]);
            lines.push(triangles[(i + 1) % triangles.length]);
        }

        this.elements.lines = lines;
    }

    updateBuffers() {
        for (let mode in this.elements) {
            this.elements[mode]
        }
    }

    draw(shader, mode = "triangles") {

        // Select primitive mode
        let primitive = undefined;
        switch (mode) {
            case "triangles": primitive = GL.TRIANGLES; break;
            case "lines": primitive = GL.LINES; break;
        }

        // 
        if (primitive === undefined) throw new Error("Unable to draw mesh, invalid mode.");
        else {

            const buffers = this.buffers;

            // Configure Attributes 
            for (let name in buffers.vertex) {
                let vItem = buffers.vertex[name];

                // If needed, update buffer.
                if (vItem.changed) {
                    // console.log(`Updating Vertices: ${name}`);
                    vItem.buffer.update(this.vertices[name]);
                    vItem.changed = false;
                }

                // 
                let size = GLSLTypeSize[this.layout[vItem.index].type];

                // 
                // Bind vertex buffer
                GL.bindBuffer(GL.ARRAY_BUFFER, vItem.buffer.id);
                GL.vertexAttribPointer(vItem.index, size, GL.FLOAT, false, 0, 0);
                GL.enableVertexAttribArray(vItem.index);
            }

            // Bind Element Buffer 
            let elementBuffer = buffers.element[mode];
            if (elementBuffer.changed) {
                elementBuffer.buffer.update(this.elements[mode]);
                elementBuffer.changed = false;
            }

            // Bind vertex buffer
            GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, elementBuffer.buffer.id);

            // Enable shader 
            shader.enable();

            // Draw Mesh
            GL.drawElements(primitive, this.elements[mode].length, GL.UNSIGNED_SHORT, 0);

            // Unbind buffers 
            GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, null);
            GL.bindBuffer(GL.ARRAY_BUFFER, null);
        }
    }

    /** 
     * Constructs a simple quad mesh, oriented on the xy-plane.
     */
    static CreateSimpleQuad() {

        // Create a simple quad mesh
        var mesh = new Mesh();
        mesh.vertices.positions = [-1.0, -1.0, 0.0, +1.0, -1.0, 0.0, +1.0, +1.0, 0.0, -1.0, +1.0, 0.0,];
        mesh.vertices.normals = [0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0];
        mesh.vertices.uvs = [0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0];
        mesh.elements.triangles = [0, 1, 2, 0, 2, 3];
        mesh.computeWireframe();

        return mesh;
    }

    /**
     * Creates a sphere
     */
    static CreateSphere(stacks = 6, slices = 12) {

        let positions = [];
        let normals = [];
        let uvs = [];

        let triangles = [];

        const inc = Math.PI / 4;

        const cos = Math.cos, sin = Math.sin;
        const append = (array, values) => Array.prototype.push.apply(array, values);

        // 
        for (let p1 = 0; p1 < slices; p1++) {
            let pA = (p1 / slices) * 2.0 * Math.PI;

            // 
            for (let t1 = 0; t1 < stacks; t1++) {
                let tA = (t1 / stacks) * Math.PI;

                //
                let x = sin(tA) * cos(pA);
                let y = sin(tA) * sin(pA);
                let z = cos(tA);

                // Append Vertex Set
                append(uvs, [p1 / slices, t1 / stacks]);
                append(positions, [x, y, z]); // * radius
                append(normals, [x, y, z]);

                // Map Triangles
                let t2 = t1 === (stacks - 1) ? 0 : t1 + 1;
                let p2 = p1 === (slices - 1) ? 0 : p1 + 1;

                let idx0 = p1 * stacks + t1;
                let idx1 = p2 * stacks + t1;
                let idx2 = p1 * stacks + t2;
                let idx3 = p2 * stacks + t2;

                // Append Quad
                append(triangles, [idx0, idx1, idx2]);
                append(triangles, [idx2, idx1, idx3]);
            }
        }

        // Create a simple quad mesh
        var mesh = new Mesh();
        mesh.elements.triangles = triangles;
        mesh.vertices.positions = positions;
        mesh.vertices.normals = normals;
        mesh.vertices.uvs = uvs;
        mesh.computeWireframe();

        return mesh;
    }
}