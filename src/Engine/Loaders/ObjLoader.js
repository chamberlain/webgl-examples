import Loader from '../Loader.js';
import { Mesh } from '../Mesh.js';

function constructOpenGLMeshes(model) {

    var data = [];

    // For each OBJ Object
    model.objects.forEach(obj => {

        var triangles = [];
        var positions = [];
        var normals = [];
        var uvs = [];

        let index = 0;

        // Each face in object
        for (var face of obj.faces) {

            // Each vertex in face
            for (var f of face) {
                // 
                Array.prototype.push.apply(positions, model.positions[f.v - 1]);
                Array.prototype.push.apply(normals, model.normals[f.n - 1]);
                Array.prototype.push.apply(uvs, model.uvs[f.t - 1]);

                // 
                triangles.push(index++);
            }
        }

        var mesh = new Mesh();

        //
        mesh.vertices.positions = positions;
        mesh.vertices.normals = normals;
        mesh.vertices.uvs = uvs;

        //
        mesh.elements.triangles = triangles;
        mesh.computeWireframe();

        data.push({
            name: obj.name,
            material: obj.material,
            mesh: mesh
        });

    }, this);

    return data;
}

export default {

    Load(path, progress) {

        let load = Loader.Get(path, "text", progress);

        return new Promise((resolve, reject) => {

            load.then(text => {

                let library = undefined;
                let current = undefined;

                let model = {
                    positions: [],
                    normals: [],
                    uvs: [],
                    library: undefined,
                    objects: []
                };

                let lineIndex = 0;
                let lines = text.split('\n');
                while (lineIndex < lines.length) {
                    let line = lines[lineIndex++].trim();

                    // Skip comments
                    if (line.startsWith("#"))
                        continue;

                    // 
                    let segments = line.split(' ');

                    switch (segments[0]) {

                        case "mtllib":
                            model.library = segments[1];
                            break;

                        case "usemtl":
                            current.material = segments[1];
                            break;

                        case "o":
                            model.objects.push(current = {
                                name: segments[1],
                                faces: []
                            });
                            break;

                        case "v":
                            //
                            var x = parseFloat(segments[1]);
                            var y = parseFloat(segments[2]);
                            var z = parseFloat(segments[3]);

                            // Vertex position definition 
                            model.positions.push([x, y, z]);
                            break;

                        case "vn":
                            //
                            var x = parseFloat(segments[1]);
                            var y = parseFloat(segments[2]);
                            var z = parseFloat(segments[3]);

                            // Vertex normal definition
                            model.normals.push([x, y, z]);
                            break;

                        case "vt":
                            //
                            var x = parseFloat(segments[1]);
                            var y = parseFloat(segments[2]);

                            // Vertex uv definition
                            model.uvs.push([x, y]);
                            break;

                        case "f":
                            let face = [];
                            for (var i = 1; i < segments.length; i++) {
                                var indices = segments[i].split('/');
                                face.push({
                                    v: +indices[0],
                                    t: +indices[1],
                                    n: +indices[2]
                                });
                            }

                            // Triangulate
                            for (var i = 0; i < face.length - 2; i++)
                                current.faces.push([face[0], face[i + 1], face[i + 2]]);

                            break;
                    }
                }

                // 
                let data = constructOpenGLMeshes(model);
                resolve(data);
            });

            // Failure
            load.catch(reject);
        });
    }
}