import Loader from '../Loader.js';
import Texture from '../Texture.js';
import { Mesh } from '../Mesh.js';

export default {

    Load(path, progress) {

        let load = Loader.Get(path, "blob", progress);

        return new Promise((resolve, reject) => {

            load.then(blob => {

                // Load image into browser context
                let image = new Image();
                image.src = window.URL.createObjectURL(blob);
                image.onload = () => {
                    // Success, we have a texture.
                    resolve(new Texture(image));
                }
            });

            // Failure
            load.catch(reject);
        });
    }
}