import { GL } from "./Context.js";

/**
 * A buffer that holds GPU vertex data.
 */
// TODO: Buffer hint ( static, dynamic, stream )
export default class VertexBuffer {

    /**
     * Create a new vertex buffer.
     * @param {*} data Some initial data.
     */
    constructor(data = undefined) {
        this.id = GL.createBuffer();
        if (data) this.update(data);
    }

    /**
     * Update the buffer data.
     * @param {*} data An array of data to populate the buffer
     * @param {*} type Friendly name of the GL type this buffer holds
     */
    update(data, type = "float") {

        // TODO: Allow other data types
        if (type !== "float") throw new Error("Only float data allowed currently");

        // Wrap as Float32
        if (data instanceof Float32Array == false)
            data = new Float32Array(data);

        // Bind and update buffer
        GL.bindBuffer(GL.ARRAY_BUFFER, this.id);
        GL.bufferData(GL.ARRAY_BUFFER, data, GL.STATIC_DRAW);
        GL.bindBuffer(GL.ARRAY_BUFFER, null);
    }
}