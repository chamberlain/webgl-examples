// Clamp the list positions to allow looping
function wrapList(pos, size) {

    while (pos < 0)
        pos += size;


    while (pos >= size)
        pos -= size;

    return pos;
}


//Returns a position between 4 Vector3 with Catmull-Rom spline algorithm
//http://www.iquilezles.org/www/articles/minispline/minispline.htm
function getCatmullRomPosition(t, p0, p1, p2, p3) {
    // The coefficients of the cubic polynomial (except the 0.5f * which I added later for performance)
    let a = 2 * p1;
    let b = p2 - p0;
    let c = 2 * p0 - 5 * p1 + 4 * p2 - p3;
    let d = -p0 + 3 * p1 - 3 * p2 + p3;

    // The cubic polynomial: a + b * t + c * t^2 + d * t^3
    return 0.5 * (a + (b * t) + (c * t * t) + (d * t * t * t));
}

// http://www.habrador.com/tutorials/interpolation/1-catmull-rom-splines/
export default class CatmullRomSpline {

    constructor() {
        this._points = [];
    }

    /**
     * Append a control point on the curve.
     * @param {Number} value Some x-coordinate value.
     */
    addPoint(value) {
        this._points.push(value);
    }

    /**
     * Evaluate an interpolated value along the curve.
     * @param {Number} time A 0 - n value where n is the number of points in the curve.
     */
    eval(time) {

        let frame = Math.floor(time);
        let t = time - frame;

        // The 4 points we need to form a spline between p1 and p2
        let p0 = this._points[wrapList(frame - 1, this._points.length)];
        let p1 = this._points[wrapList(frame + 0, this._points.length)];
        let p2 = this._points[wrapList(frame + 1, this._points.length)];
        let p3 = this._points[wrapList(frame + 2, this._points.length)];

        // 
        return getCatmullRomPosition(t, p0, p1, p2, p3);
    }
}