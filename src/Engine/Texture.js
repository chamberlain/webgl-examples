import { GL } from "./Context.js";

const isPowerOf2 = (value) => {
    return (value & (value - 1)) == 0;
};

/**
 * A 2D Texture
 */
export default class Texture {

    /**
     * Creates a new 2D Texture.
     * @param {Image} image Some HTML image
     */
    constructor(image) {
        this.texture = GL.createTexture();
        if (image) this.update(image);
    }

    /**
     * Binds the current texture the given unit.
     * @param {Number} [unit=0] Texture Unit
     */
    use(unit = 0) {
        GL.activeTexture(GL.TEXTURE0 + unit);
        GL.bindTexture(GL.TEXTURE_2D, this.texture);
    }

    /**
     * Updates the texture with new data.
     */
    update(image) {

        GL.bindTexture(GL.TEXTURE_2D, this.texture);
        GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, GL.RGBA, GL.UNSIGNED_BYTE, image)

        // Create mimaps
        if (isPowerOf2(image.width) && isPowerOf2(image.height))
            GL.generateMipmap(GL.TEXTURE_2D);

        GL.bindTexture(GL.TEXTURE_2D, null);
    }
}