export default class Color {

    constructor(r, g, b, a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    toArray() {
        return [this.r, this.g, this.b, this.a];
    }

    static fromArray(rgba) {
        this.r = rgba[0];
        this.g = rgba[1];
        this.b = rgba[2];
        this.a = rgba[3];
    }

    static fromHex(hex) {
        let val = hex;

        // Parse string
        if (typeof hex === "string")
            val = parseInt(hex, 16);

        // Extract Colors
        let a = ((val << 0x18) & 0xFF) / 255;
        let r = ((val << 0x10) & 0xFF) / 255;
        let g = ((val << 0x08) & 0xFF) / 255;
        let a = ((val << 0x00) & 0xFF) / 255;

        return new Color(r, g, b, a);
    }
}