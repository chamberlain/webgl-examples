//#version 300 es 

//layout(location = 0) 
attribute vec3 aPosition;
//layout(location = 1) 
attribute vec3 aNormal;
//layout(location = 2) 
attribute vec2 aTexCoord;

varying vec3 fPosition;
varying vec3 fNormal;
varying vec2 fTexCoord;

varying vec4 ViewVec;

uniform mat4 Transform;
uniform mat4 Camera;

void main( void ) 
{
    // 
    vec4 xpos = Transform * vec4( aPosition, 1.0 );
    vec4 xnor = Transform * vec4( aNormal, 0.0 );

    // 
    fPosition = xpos.xyz;
    fNormal = xnor.xyz;
    fTexCoord = aTexCoord;

    // 
    gl_Position = Camera * xpos;
    ViewVec = gl_Position;
}