// #version 300 es
precision mediump float;

#define GLOBAL_BRIGHTNESS 0.5;

varying vec3 fPosition;
varying vec3 fNormal;
varying vec2 fTexCoord;

varying vec4 ViewVec;

uniform vec3 CameraPos;
uniform sampler2D Skybox;
uniform sampler2D Albedo;
uniform vec3 Blend;

float toGrayscale( in vec3 c ) {
    return  dot( c, vec3( 0.299, 0.587, 0.114 ) );
}

float phong( in vec3 N, in vec3 L ) {
    return max( 0.0, dot( N, L ) );
}

float fog( const float dist,  const float density ) {
    const float LOG2 = -1.442695;
    float d = density * dist;
    return  1.0 - clamp(exp2(d * d * LOG2), 0.0, 1.0);
}

vec3 desaturate( in vec3 c, in float saturation ) {
    return c * saturation + toGrayscale( c ) * ( 1.0 - saturation );
}

/** Computes a texture coordinate for the skydome */
vec2 skyboxCoord( in vec3 pos ) {
    vec3 n = normalize( pos );
    float h = ( atan( n.y, -n.x ) + 3.141 ) / 6.283;
    return vec2( h, n.z );
}

/** Samples the skydome */
vec3 getSkyboxColor( in vec3 pos ) {
    vec2 sky = skyboxCoord( pos );
    if( pos.z < 0.0 ) sky.y *= -1.0;
    return texture2D( Skybox, sky ).rgb * GLOBAL_BRIGHTNESS;
}

void main() 
{
    vec3 V = normalize( ViewVec.xyz );
    vec3 L = normalize( vec3( 2.0, 0.5, 1.0 ) );
    vec3 N = normalize( fNormal );
    vec3 R = reflect( L, N );
    vec3 Q = reflect( V, N );

    // 
    vec3 diff = vec3( phong( N, L ) ) * GLOBAL_BRIGHTNESS;
    vec3 spec = vec3( pow( phong( -R, V ), 50.0 ) ) * GLOBAL_BRIGHTNESS;

    vec3 texel = texture2D( Albedo, fTexCoord ).rgb;
    if( N.z > 0.8 ) texel = vec3( 0.0 );
    
    // Is a window?
    bool isPanel = length(texel) > 0.075;

    // Surface color
    vec3 surface = texel;
    if( isPanel ) { 
        // Compute reflective windows
        vec3 C = normalize( fPosition - CameraPos );
        spec += getSkyboxColor( reflect( C, N ) );
        surface /= 2.0;
    }

    // Create final color
    vec3 color = surface * diff + spec;

    // Apply window lumenance
    color += pow( texel, vec3( 2.2 ) ) * 1.25;

    vec3 SKY_COLOR = getSkyboxColor( fPosition - CameraPos ); // vec3( 0.15, 0.12, 0.09 );
    const vec3 GROUND_COLOR = vec3( 0.6, 0.8, 0.9 ) * 0.7;

    // Ground fog
    float groundFogAmount = 1.0 - fog( fPosition.z, 0.33 );
    color.rgb = mix( color.rgb, GROUND_COLOR, groundFogAmount );

    // Sky fog
    float fogAmount = fog( gl_FragCoord.z / gl_FragCoord.w, 0.03 );
    color.rgb = mix( color.rgb, SKY_COLOR, fogAmount );

    // 
    color *= Blend;

    // Desaturate
    color = desaturate( color, 1.0 );

    // Write final color
    gl_FragColor = vec4( color, 1.0 );

    // TO VISUALIZE NORMALS
    // gl_FragColor = vec4( N * 0.5 + 0.5, 1.0 );

    // TO VISUALIZE TEXCOORD
    // gl_FragColor = vec4( fTexCoord, 0.0, 1.0 );

    // TO VISUALIZE VIEW VEC
    // gl_FragColor = vec4( V, 1.0 );

    // TO VISUALIZE WORLD POSITION
    // gl_FragColor = vec4( fPosition, 1.0 );
}
